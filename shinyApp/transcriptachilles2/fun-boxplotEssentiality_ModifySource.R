## ==================================================================================== ##
# transcriptAchilles Shiny App for predicting transcript biomarkers.
# Copyright (C) 2018
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# You may contact the author of this code, Fernando Carazo, at <fcarazo@tecnun.es>
## ==================================================================================== ##



boxplotEssentiality <- function(Essentiality, iCL, Gene_E, tag = "", co_Ess = -2){
  
  # Check if Gene_E is defined, if it is not, return and wait for its definition
  if (length(Gene_E) == 0){
    return()
  }
  
  # Example test
  # length(iCL) = 14 samples/cell lines of different kidney cancer subtypes
  # cl is a string vector (description) for each cell line
  # ncol(Essentiality) = 412 cl | rest of CLs = 412 - 14 = 398
  cl <- rep(sprintf("Rest CLs (n = %s)", ncol(Essentiality) - length(iCL)), ncol(Essentiality)) # Repeat 'Rest CLs ...' the number of cols of Essentiality (#cell lines = 412)
  # In the 14 samples/cell lines defined by the indices iCL
  cl[iCL] <- sprintf("Selected CLs (n = %s)", length(iCL)) # iCL is the index of the current selected gene
  cl <- as.factor(cl)
  # Put 'Selected CLs (n = %s)' in the first level (# = 1)
  cl <- relevel(cl, ref = sprintf("Selected CLs (n = %s)", length(iCL)))

  
  
  # Testing
  # cat(file=stderr(),dim(Essentiality),iCL," ",cl[iCL]," ",length(cl)," ",Gene_E," ",length(Essentiality[Gene_E, ])," ",typeof(cl)," ", typeof(Essentiality),"\n")
  
  
  boxplot(Essentiality[Gene_E, ] ~ cl,   # Resolve it --> Warning: Error in model.frame.default: variable lengths differ (found for 'cl')
          main = sprintf("Essentiality score of %s",Gene_E),
          xlab = paste("Cell Lines"), 
          ylab = paste("Essentiality score of ", Gene_E, " (DEMETER)"))
  abline(h = co_Ess, lty = 2, col = 2)
  par(mfrow = c(1, 1))
}


