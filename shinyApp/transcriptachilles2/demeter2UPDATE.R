# DEMETER2 DATABASE UPDATE

# Clean workspace
rm(list = ls())

library(rstudioapi)
# Set working directory
setwd(dirname(getActiveDocumentContext()$path))


# RData needs to be update in order to contain the new DEMETER2 scores
# Load data ---------------------------------------------------------------
# Loads Cancer Cell Line Encyclopedia (CCLE) data as: 
# 1.- demeter_match matrix (demeter score per each gene (17098 rows) and each cell line (412 columns))
# 2.- sample_info_match matrix (cell line data/info -5 columns & + binary mutation info- associated to each cell line - 412 rows/cell lines)
# 3.- tpm_gn_match matrix (expresion per each gene (60554 rows) and each cell line (412 columns)) ???
# 4.- tpm_match matrix (expresion per each transcript (199169 rows) and each cell line (412 columns)) ???
# 5.- getBM matrix (transcript and gene id (128375 rows) and more info about them (6 columns))

# Load all data from Transcript Achilles 1
load("dataShiny/DataForShiny2.RData")

# CCLE_demeter_match ---------------------------------------------------------------------------------------------
dim(CCLE_demeter_match) # 17098 rows/genes | 412 columns/cell lines
min(CCLE_demeter_match) # -14.7822
max(CCLE_demeter_match) # 15.5321
# Check if not repeated genes
nameRowsDM <- rownames(CCLE_demeter_match)
nameRowsDM_unique <- unique(nameRowsDM)
length(nameRowsDM) # 17098
length(nameRowsDM_unique) # 17098 
# Check if not repeated cell lines
nameColsDM <- colnames(CCLE_demeter_match)
nameColsDM_unique <- unique(nameColsDM)
length(nameColsDM) # 412
length(nameColsDM_unique) # 412
# Check if the matrix contains NA
sum(is.na(CCLE_demeter_match)) # 0
# Count elements not NA in matrix
sum(is.na(CCLE_demeter_match)==FALSE) # 7044376



# CCLE_sample_info_match ---------------------------------------------------------------------------------------------
dim(CCLE_sample_info_match) #  412 rows/cell lines | 1707 columns/cell lines info (5) & binary gene mutations (1702)
# min(CCLE_sample_info_match) # NA
# max(CCLE_sample_info_match) # NA
# Check if not repeated cell lines
nameRowsSIM <- rownames(CCLE_sample_info_match)
nameRowsSIM_unique <- unique(nameRowsSIM)
length(nameRowsSIM) # 412
length(nameRowsSIM_unique) # 412
# Check if not repeated genes
nameColsSIM <- colnames(CCLE_sample_info_match)
nameColsSIM_unique <- unique(nameColsSIM)
length(nameColsSIM) # 1707
length(nameColsSIM_unique) # 1707
# Check if the data.frame contains NA
sum(is.na(CCLE_sample_info_match[,-(1:5)])) # 75337
# Count elements not NA in data.frame
sum(is.na(CCLE_sample_info_match[-(1:5)])==FALSE) # 625887
dim(CCLE_sample_info_match[-(1:5)])[1]*dim(CCLE_sample_info_match[-(1:5)])[2] # 701224 


# CCLE_tpm_gn_match ---------------------------------------------------------------------------------------------
dim(CCLE_tpm_gn_match) # 60554 rows/genes | 412 columns/cell lines
min(CCLE_tpm_gn_match) # 0
max(CCLE_tpm_gn_match) # 110304
# Check if not repeated transcripts
nameRowsTGM <- rownames(CCLE_tpm_gn_match)
nameRowsTGM_unique <- unique(nameRowsTGM)
length(nameRowsTGM) # 60554
length(nameRowsTGM_unique) # 60554
# Check if not repeated cell lines
nameColsGM <- colnames(CCLE_tpm_gn_match)
nameColsGM_unique <- unique(nameColsGM)
length(nameColsGM) # 412
length(nameColsGM_unique) # 412
# Check if the matrix contains NA
sum(is.na(CCLE_tpm_gn_match)) # 0
# Count elements not NA in matrix
sum(is.na(CCLE_tpm_gn_match)==FALSE) # 24948248



# CCLE_tpm_match ---------------------------------------------------------------------------------------------
dim(CCLE_tpm_match) # 199169 rows/transcripts (Ensembl ID) | 412 columns/cell lines
min(CCLE_tpm_match) # 0
max(CCLE_tpm_match) # 110304
# Check if not repeated transcripts
nameRowsTM <- rownames(CCLE_tpm_match)
nameRowsTM_unique <- unique(nameRowsTM)
length(nameRowsTM) # 199169
length(nameRowsTM_unique) # 199169
# Check if not repeated cell lines
nameColsTM <- colnames(CCLE_tpm_match)
nameColsTM_unique <- unique(nameColsTM)
length(nameColsTM) # 412
length(nameColsTM_unique) # 412
# Check if the matrix contains NA
sum(is.na(CCLE_tpm_match)) # 0
# Count elements not NA in matrix
sum(is.na(CCLE_tpm_match)==FALSE) # 82057628


# getBM ---------------------------------------------------------------------------------------------
dim(getBM) # 199169 rows/transcripts | 5 columns/Transcript info
# min(getBM) # NA
# max(getBM) # NA
# Check if not repeated transcript ID
nameRowsGBM <- getBM$Transcript_ID
nameRowsGBM_unique <- unique(nameRowsGBM)
length(nameRowsGBM) # 199169
length(nameRowsGBM_unique) # 199169
# Check if not repeated transcript info field
nameColsGBM <- colnames(getBM)
nameColsGBM_unique <- unique(nameColsGBM)
length(nameColsGBM) # 5
length(nameColsGBM_unique) # 5
# Check if the matrix contains NA
sum(is.na(getBM)) # 0
# Count elements not NA in matrix
sum(is.na(getBM)==FALSE) # 995845


# Testing
# load("dataShiny/DataForShiny2_opt.RData")
# load("dataShiny/DataForShiny2_opt_small.RData")


# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------


# DEMETER2 data --------------------------------------------------------------------------
#### DEMETER2 DATA for mutation and copy number ####
demeter2_mutation <- read.csv("./demeter2FILES/CCLE_mutation_data.csv")
dim(demeter2_mutation)
demeter2_gene_level_copy_number <- read.csv("./demeter2FILES/WES_SNP_CN_data.csv")
dim(demeter2_gene_level_copy_number)
#### end ####

#### DEMETER2 scores data ####
# DEMETER2 scores match with genes and cell lines ------------------------------------
# demeter2_scores <- read.csv("./demeter2FILES/D2_Achilles_gene_dep_scores.csv",check.names=FALSE) # 16755 rows | 502 columns/cell lines
# demeter2_scores <- read.csv("./demeter2FILES/D2_DRIVE_gene_dep_scores.csv",check.names=FALSE) # 7975 rows | 398 columns/cell lines

# Load combined database scores (Achilles + DRIVE + Marcotte)
demeter2_scores <- read.csv("./demeter2FILES/D2_combined_gene_dep_scores.csv",check.names=FALSE) # 17309 rows | 713 columns/cell lines


nameRowsD2S <- demeter2_scores[,1] # Gene names DEMETER2 - 17309
# Remove " (1...)" from demeter2_scores row names (genes name)
nameRowsD2S_fixed <- sapply(strsplit(as.character(nameRowsD2S), split=' ', fixed=TRUE), function(x) (x[1]))
rownames(demeter2_scores) <- nameRowsD2S_fixed
demeter2_scores <- demeter2_scores[,-1]

demeter2_scores <- demeter2_scores[order(rownames(demeter2_scores),decreasing = FALSE),] # Order by genes name
demeter2_scores <- demeter2_scores[,order(colnames(demeter2_scores),decreasing = FALSE)] # Order by cell lines name
demeter2_scores <- as.matrix(demeter2_scores)

# Check demeter2_scores size and min and max values
dim(demeter2_scores) # Achilles only (16755 rows/genes | 502 columns/cell lines) | *Combined (17309 rows/genes | 712 columns/cell lines)
min(demeter2_scores,na.rm = TRUE) # NA
max(demeter2_scores, na.rm = TRUE) # NA
min(demeter2_scores,na.rm=TRUE) # Achilles (-2.657362) | Combined (-5.932379)
max(demeter2_scores,na.rm=TRUE) # Achilles (1.7028) | Combined (2.774956)

# Check if not repeated transcripts
nameRowsD2S <- rownames(demeter2_scores)
nameRowsD2S_unique <- unique(nameRowsD2S)
length(nameRowsD2S) # 17309
length(nameRowsD2S_unique) # 17309
# Check if not repeated cell lines
nameColsD2S <- colnames(demeter2_scores)
nameColsD2S_unique <- unique(nameColsD2S)
length(nameColsD2S) # 712
length(nameColsD2S_unique) # 712
# Check if the matrix contains NA
sum(is.na(demeter2_scores)) # 2772124
# Count elements not NA in matrix
sum(is.na(demeter2_scores)==FALSE) # 9551884
dim(demeter2_scores)[1]*dim(demeter2_scores)[2] # 12324008
#### end ####

dim(demeter2_scores) # 17309 rows/genes | 712 columns/cell lines


# Load all expression data from Ferca (CCLE_sample_info_all, CCLE_tpm_gn_all, CCLE_tpm_all) ----------------------------------------
load("./transcriptAchilles1DataFerCa/CCLE_expression_allCLS.RData")

# load("D:/softwareGIT/transcriptachilles2/dataAll/input/CCLE_sample_info_all.RData")
# load("D:/softwareGIT/transcriptachilles2/dataAll/input/CCLE_tpm_all.RData")
# load("D:/softwareGIT/transcriptachilles2/dataAll/input/CCLE_tpm_gn_allGenes.RData")
# CCLE_tpm_gn_all <- CCLE_tpm_gn

dim(CCLE_sample_info_all) # cl(923) 1707
dim(CCLE_tpm_gn_all) # genes(60554) cl(924)
dim(CCLE_tpm_all) # 199169 cl(924)

# Check if all tables have unique row and column names !!!!!!!!!!!!!!!!!!!!!!!!!! TODO!!!!!!!!!!!!!!
# CCLE_sample_info_all ---------------------------------------------------------------------------------------------
dim(CCLE_sample_info_all) #  923 rows/cell lines |  columns/cell lines info (5) & binary gene mutations (1702)
# min(CCLE_sample_info_all) # NA
# max(CCLE_sample_info_all) # NA
# Check if not repeated cell lines
nameRowsSIM <- rownames(CCLE_sample_info_all)
nameRowsSIM_unique <- unique(nameRowsSIM)
length(nameRowsSIM) # 923
length(nameRowsSIM_unique) # 923
# Check if not repeated genes
nameColsSIM <- colnames(CCLE_sample_info_all)
nameColsSIM_unique <- unique(nameColsSIM)
length(nameColsSIM) # 1707
length(nameColsSIM_unique) # 1707
# Check if the data.frame contains NA
sum(is.na(CCLE_sample_info_all[,-(1:5)])) # 148128
# Count elements not NA in data.frame
sum(is.na(CCLE_sample_info_all[-(1:5)])==FALSE) # 1422818
dim(CCLE_sample_info_all[-(1:5)])[1]*dim(CCLE_sample_info_all[-(1:5)])[2] # 1570946  


# CCLE_tpm_gn_all ---------------------------------------------------------------------------------------------
dim(CCLE_tpm_gn_all) # 60554 rows/genes | 924 columns/cell lines
gnAux <- CCLE_tpm_gn_all$Gene_ID
rownames(CCLE_tpm_gn_all) <- gnAux
CCLE_tpm_gn_all <- CCLE_tpm_gn_all[,-1] 
dim(CCLE_tpm_gn_all) # 60554 rows/genes | 923 columns/cell lines
iGN <- match(rownames(CCLE_tpm_gn_all), getBM$Gene_ID) # Vector of indices indicating where is each element of A in B

numGenes <- getBM$Gene_name[iGN]
length(unique(numGenes))


row_names_CCLE_tpm_gn_all <- make.unique(getBM$Gene_name[iGN])
rownames(CCLE_tpm_gn_all) <- row_names_CCLE_tpm_gn_all
CCLE_tpm_gn_all <- as.matrix(CCLE_tpm_gn_all)

min(CCLE_tpm_gn_all) # 0
max(CCLE_tpm_gn_all) # 375418
# Check if not repeated transcripts
nameRowsTGM <- rownames(CCLE_tpm_gn_all)
nameRowsTGM_unique <- unique(nameRowsTGM)
length(nameRowsTGM) # 60554
length(nameRowsTGM_unique) # 60554
# Check if not repeated cell lines
nameColsGM <- colnames(CCLE_tpm_gn_all)
nameColsGM_unique <- unique(nameColsGM)
length(nameColsGM) # 923
length(nameColsGM_unique) # 923
# Check if the matrix contains NA
sum(is.na(CCLE_tpm_gn_all)) # 0
# Count elements not NA in matrix
sum(is.na(CCLE_tpm_gn_all)==FALSE) # 55830788




# CCLE_tpm_all ---------------------------------------------------------------------------------------------
dim(CCLE_tpm_all) # 199169 rows/transcripts (Ensembl ID) | 924 columns/cell lines
nameRowsCTM0 <- as.matrix(CCLE_tpm_all[,1]) # Transcript id -> Move column #1 to rowname 
nameRowsCTM <- sapply(strsplit(as.character(nameRowsCTM0), split='.', fixed=TRUE), function(x) (x[1]))
nameRowsCTM_unique <- unique(nameRowsCTM)
length(nameRowsCTM) # 199169 
length(nameRowsCTM_unique) # 199169
CCLE_tpm_all <- CCLE_tpm_all[,-1] # This removes rownames ...
rownames(CCLE_tpm_all) <- nameRowsCTM # Transcripts
# 199169 rows/transcripts (Ensembl ID) | 923 columns/cell lines
CCLE_tpm_all <- as.matrix(CCLE_tpm_all)

min(CCLE_tpm_all) # 0
max(CCLE_tpm_all) # 375418
# Check if not repeated transcripts
nameRowsTM <- rownames(CCLE_tpm_all)
nameRowsTM_unique <- unique(nameRowsTM)
length(nameRowsTM) # 199169
length(nameRowsTM_unique) # 199169
# Check if not repeated cell lines
nameColsTM <- colnames(CCLE_tpm_all)
nameColsTM_unique <- unique(nameColsTM)
length(nameColsTM) # 923
length(nameColsTM_unique) # 923
# Check if the matrix contains NA
sum(is.na(CCLE_tpm_all)) # 0
# Count elements not NA in matrix
sum(is.na(CCLE_tpm_all)==FALSE) # 183832987

# stop("bye bye")

# -----------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------
# Columns/cell lines matching (Ferca data & DEMETER2 data)  -------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------
# Match with DEMETER2 ----------------------------------------------------------
nameColumnsD2S_ALL <- colnames(demeter2_scores) # DEMETER2 data -> CCLE.name (long)

# CCLE_sample_info_all ------------------------------------------------------------------------
CCLE_sample_info_all <- CCLE_sample_info_all[order(CCLE_sample_info_all$CCLE.name,decreasing = FALSE),] # Order by CCLE.name in rows

nameRowsSI_ALL <- CCLE_sample_info_all$CCLE.name # Cell lines - Ferca data -> CCLE.name (long)
nameRowsSI_ALL_unique <- unique(nameRowsSI_ALL)

maskMatchColumns <- nameRowsSI_ALL %in% nameColumnsD2S_ALL # Vector of 1 & 0 indicating if A is included in B
cl_matched <- nameRowsSI_ALL[maskMatchColumns]
length(cl_matched) # 568 cell lines matched <----------------------------------------------------------------
# maskMatchColumns <- nameColumnsD2S_ALL %in% nameRowsSI_ALL # Vector of 1 & 0 indicating if A is included in B
# cl_matched <- nameColumnsD2S_ALL[maskMatchColumns]

maskMatchRowsIndicesIn_D2S_ALL <- match(nameRowsSI_ALL,nameColumnsD2S_ALL,nomatch = 0) # Vector of indices indicating where is each element of A in B
maskMatchRowsIndicesIn_sample_info_all <- match(nameColumnsD2S_ALL, nameRowsSI_ALL, nomatch = 0) # Vector of indices indicating where is each element of A in B


maskNotMatchRows <- !(nameRowsSI_ALL %in% cl_matched)
cl_not_matched <- nameRowsSI_ALL[maskNotMatchRows]
length(cl_not_matched) # 335

maskMatchRowsIndicesSIM <- maskMatchRowsIndicesIn_sample_info_all # Cell lines matched with DEMETER2
#nameRowsSI_ALL[maskMatchRowsIndicesSIM]












stop("bye bye") # -----------------------------------------------------------------------------------------------------------------




















# output --> cl_matched & cl_not_matched -------------------------------------------

# Matrix setup --------------------------------------------------------------------------------
CCLE_tpm_gn_all_log2 <- log2(1+CCLE_tpm_gn_all)
#rownames(CCLE_tpm_gn_all_log2) <- rownames(CCLE_tpm_gn_all)


# CCLE_tpm_gn_all -----------------------------------------------------------------------------
# Match with DEMETER2 ----------------------------------------------------------
nameColumnsTG_ALL <- colnames(CCLE_tpm_gn_all) # Cell lines - Ferca data -> CCLE.name (long)
maskMatchColumnsTG_ALL <- nameColumnsTG_ALL %in% nameColumnsD2S_ALL # Vector of 1 & 0 indicating if A is include in B
cl_matched2 <- nameColumnsTG_ALL[maskMatchColumnsTG_ALL]
maskMatchColumnsIndicesIn_D2S_ALL <- match(nameColumnsTG_ALL, nameColumnsD2S_ALL,nomatch = 0) # Vector of indices indicating where is each element of A in B
maskMatchColumnsIndicesIn_tmp_gn_all <- match(nameColumnsD2S_ALL, nameColumnsTG_ALL, nomatch = 0) # Vector of indices indicating where is each element of A in B

length(cl_matched2) # 568 cell lines matched <----------------------------------------------------------------

maskMatchColumnsIndicesTGM <- maskMatchColumnsIndicesIn_tmp_gn_all # Cell lines matched with DEMETER2


# Match with DEMETER2 ----------------------------------------------------------
nameColumnsTM_ALL <- colnames(CCLE_tpm_all) # Cell lines - Ferca data
maskMatchColumnsTM <- nameColumnsTM_ALL %in% nameColumnsD2S_ALL # Vector of 1 & 0 indicating if A is include in B
cl_matched3 <- nameColumnsTM_ALL[maskMatchColumnsTM]
maskMatchColumnsIndicesIn_D2S_ALL <- match(nameColumnsTM_ALL,nameColumnsD2S_ALL,nomatch = 0) # Vector of indices indicating where is each element of A in B
maskMatchColumnsIndicesIn_tmp_all <- match(nameColumnsD2S_ALL, nameColumnsTM_ALL, nomatch = 0) # Vector of indices indicating where is each element of A in B
length(cl_matched3) # 568 cell lines matched <----------------------------------------------------------------

maskMatchColumnsIndicesTM <- maskMatchColumnsIndicesIn_tmp_all # Cell lines matched with DEMETER2



#### DEMETER2 match with CCLE_tpm_gn_all rows #### -----------------------------------------------------
          # Match between CCLE_tpm_gn_all & demeter2_scores matrices
          nameRowsTG_ALL <- rownames(CCLE_tpm_gn_all) # Genes matched
          nameColumnsD2S <- colnames(demeter2_scores) # Cell lines in DEMETER2

# Rows matching (genes) ---------------------------------------------------------------
maskMatchRows <- row_names_CCLE_tpm_gn_all %in% rownames(CCLE_demeter_match) # Vector of 1 & 0 indicating if A is include in B
genes_matched <- row_names_CCLE_tpm_gn_all[maskMatchRows] # 16940 genes matched
length(genes_matched) # 16940 genes cell lines of CCLE_demeter_match are matched in CCLE_tpm_gn_all
          
maskMatchRows <- row_names_CCLE_tpm_gn_all %in% nameRowsD2S_fixed # Vector of 1 & 0 indicating if A is include in B
genes_matched <- row_names_CCLE_tpm_gn_all[maskMatchRows] # 16521 genes matched
length(genes_matched) # 16521 genes cell lines of demeter2_scores are matched in CCLE_tpm_gn_all



maskMatchRowsIndices <- match(row_names_CCLE_tpm_gn_all,nameRowsD2S_fixed,nomatch = 0) # Vector of indices indicating where is each element of A in B
sum(maskMatchRows)
length(maskMatchRowsIndices[maskMatchRowsIndices > 0])

maskNotMatchRows <- !row_names_CCLE_tpm_gn_all %in% nameRowsD2S_fixed
genes_not_matched <- row_names_CCLE_tpm_gn_all[maskNotMatchRows] # genes not matched
length(genes_not_matched) # 44033 genes are not matched in CCLE_tpm_gn_all


# Now we have both cl_matched & genes_matched only for information -------------------------V

# -------------------------------------------------------------------------------------------
# Columns and rows matching (cell lines and genes) ------------------------------------------
# -------------------------------------------------------------------------------------------
# new_CCLE_demeter_match --------------------------------------------------------------------
length(maskMatchRowsIndices)
length(maskMatchColumnsIndicesTM)
min(maskMatchColumnsIndicesTM)
max(maskMatchColumnsIndicesTM)

# New CCLE_demeter_match table with DEMETER2 scores (only cell lines are matched)
new_CCLE_demeter_match <- demeter2_scores[,maskMatchRowsIndicesIn_D2S_ALL]
dim(new_CCLE_demeter_match)

# Sort cell lines in new_CCLE_demeter_match --------------------------------------------------------------------
new_CCLE_demeter_match <- new_CCLE_demeter_match[, order(colnames(new_CCLE_demeter_match))]
min(new_CCLE_demeter_match) # NA
max(new_CCLE_demeter_match) # NA
min(new_CCLE_demeter_match,na.rm=TRUE) # -5.932379
max(new_CCLE_demeter_match,na.rm=TRUE) # 2.381094
dim(new_CCLE_demeter_match)
#### end ####


# See genes_not_matched in the other tables --------------------------------------------------------------------
# CCLE_sample_info_all
nameColumnsCSIM <- colnames(CCLE_sample_info_all) # (1707) --> (1702) 
maskNotMatchCols1 <- genes_not_matched %in% nameColumnsCSIM # genes_not_matched () in nameColumnsCSIM (1702)
genes_not_matchedCSIM <- genes_not_matched[maskNotMatchCols1]
sum(maskNotMatchCols1) # 0 genes must be removed from CCLE_sample_info_all
# CCLE_tpm_gn_all
maskNotMatchRows <- genes_not_matched %in% nameRowsTG_ALL # genes_not_matched () in nameRowsCTGM (60554)
genes_not_matchedCTGM <- genes_not_matched[maskNotMatchRows] 
sum(maskNotMatchRows) # 0 genes must be removed from CCLE_tpm_gn_all
# getBM
# nameColumnsGBM <- getBM[,4] # (199169) # Not possible as Gene_name is constructed with make.unique
nameColumnsGBM <- rownames(CCLE_tpm_gn_all) # (199169)
maskNotMatchCols2 <- genes_not_matched %in% nameColumnsGBM # genes_not_matched () in nameColumnsGBM (199169)
maskNotMatchCols3 <- nameColumnsGBM %in% genes_not_matched # Vector of indices indicating where is each element of A in B
genes_not_matchedGBM <- genes_not_matched[maskNotMatchCols2] 
length(genes_not_matchedGBM)
sum(maskNotMatchCols2) # 44033
sum(maskNotMatchCols3) # 44033 genes must be removed from CCLE_tpm_match

# CCLE_tpm_all 
maskTranscriptsToRemove <- maskNotMatchCols3
transcripts_not_matchedCTM <- nameRowsCTM[maskTranscriptsToRemove] # 
length(transcripts_not_matchedCTM) # 44033 transcripts must be removed from CCLE_tpm_all

# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
# Remove cell lines not matched from all the matrices ------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------

# CCLE_sample_info_all --> ok
#remove <- genes_not_matchedCSIM
new_CCLE_sample_info_match <- CCLE_sample_info_all[maskMatchRowsIndicesSIM,]
#length(remove)
dim(CCLE_sample_info_all)
dim(new_CCLE_sample_info_match)


# CCLE_tpm_gn_all --> ok
# remove <- genes_not_matchedCTGM
new_CCLE_tpm_gn_match <- CCLE_tpm_gn_all[, maskMatchColumnsIndicesTGM]
#length(remove)
dim(CCLE_tpm_gn_all)
dim(new_CCLE_tpm_gn_match)


# CCLE_tpm_all --> ok
# remove <- transcripts_not_matchedCTM
new_CCLE_tpm_match <- CCLE_tpm_all[, maskMatchColumnsIndicesTM]
# rownames(new_CCLE_tpm_match) <- rownames(CCLE_tpm_all)[!rownames(CCLE_tpm_all)%in%remove]
# length(remove) # 
dim(CCLE_tpm_all)
dim(new_CCLE_tpm_match)

# getBM --> ok
# remove <- maskNotMatchCols3 # logical mask vector of column getBM[,4] --> indicates the rows to be removed
# new_getBM <- getBM[!remove,]
new_getBM <- getBM
dim(getBM)
dim(new_getBM)



# Check genes_not_matched in the new tables
# new_CCLE_sample_info_match
nameColumnsCSIM <- colnames(new_CCLE_sample_info_match) # () --> () 
maskNotMatchCols1 <- genes_not_matched %in% nameColumnsCSIM # genes_not_matched (1116) in nameColumnsCSIM ()
genes_not_matchedCSIM <- genes_not_matched[maskNotMatchCols1]
length(genes_not_matchedCSIM)
sum(maskNotMatchCols1) # 0 genes cell lines must be removed from new_CCLE_sample_info_match
# new_CCLE_tpm_gn_match
nameRowsCTGM <- rownames(new_CCLE_tpm_gn_match) # ()
maskNotMatchRows <- genes_not_matched %in% nameRowsCTGM # genes_not_matched (1116) in nameRowsCTGM ()
genes_not_matchedCTGM <- genes_not_matched[maskNotMatchRows] 
length(genes_not_matchedCTGM)
sum(maskNotMatchRows) # 0 genes cell lines must be removed from new_CCLE_tpm_gn_match
# new_CCLE_tpm_match
remove <- transcripts_not_matchedCTM
test_new_CCLE_tpm_match <- new_CCLE_tpm_match[!rownames(new_CCLE_tpm_match)%in%remove,]
dim(new_CCLE_tpm_match)
dim(test_new_CCLE_tpm_match)
dim(new_CCLE_tpm_match)[1]-dim(test_new_CCLE_tpm_match)[1]
# new_getBM
nameColumnsGBM <- new_getBM[,4] # ()
maskNotMatchCols4 <- genes_not_matched %in% nameColumnsGBM # genes_not_matched (1116) in nameColumnsGBM ()
maskNotMatchCols5 <- nameColumnsGBM %in% genes_not_matched # genes_not_matched (1116) in nameColumnsGBM ()
genes_not_matchedGBM <- genes_not_matched[maskNotMatchCols4] 
length(genes_not_matchedGBM)
sum(maskNotMatchCols4) # 0 genes cell lines must be removed from new_getBM
sum(maskNotMatchCols5) # 0 genes cell lines must be removed from new_getBM


# Order all tables by cell names & genes ----------------------------------------------------------------------------

new_CCLE_demeter_match <- new_CCLE_demeter_match[,order(colnames(new_CCLE_demeter_match))] # by cols/cell lines
new_CCLE_demeter_match <- new_CCLE_demeter_match[order(rownames(new_CCLE_demeter_match)),] # by rows/genes

new_CCLE_sample_info_match <- new_CCLE_sample_info_match[order(new_CCLE_sample_info_match$CCLE.name),] # by rows/cell lines

new_CCLE_tpm_gn_match <- new_CCLE_tpm_gn_match[,order(colnames(new_CCLE_tpm_gn_match))] # by cols/cell lines
new_CCLE_tpm_gn_match <- new_CCLE_tpm_gn_match[order(rownames(new_CCLE_tpm_gn_match)),] # by rows/genes

new_CCLE_tpm_match <- new_CCLE_tpm_match[,order(colnames(new_CCLE_tpm_match))] # by cols/cell lines
new_CCLE_tpm_match <- new_CCLE_tpm_match[order(rownames(new_CCLE_tpm_match)),] # by rows/transcripts


colnames(new_CCLE_demeter_match)[1:5]
rownames(new_CCLE_demeter_match)[1:5]

colnames(new_CCLE_sample_info_match)[1:5]
rownames(new_CCLE_sample_info_match)[1:5]
new_CCLE_sample_info_match$CCLE.name[1:5]


colnames(new_CCLE_tpm_gn_match)[1:5]
rownames(new_CCLE_tpm_gn_match)[1:5]

colnames(new_CCLE_tpm_match)[1:5]
rownames(new_CCLE_tpm_match)[1:5]

rownames(new_getBM)[1:5]
new_getBM$Transcript_ID[1:5]

# Uncomment when needs to update Rdata
save(new_CCLE_demeter_match, new_CCLE_sample_info_match, new_CCLE_tpm_gn_match, new_CCLE_tpm_match, new_getBM, file="../../data/input/demeter2FILES/new_CCLE_data_all.Rdata")

# load("../../data/input/demeter2FILES/new_CCLE_data_all.Rdata")
dim(new_CCLE_demeter_match)
dim(new_CCLE_sample_info_match)
dim(new_CCLE_tpm_gn_match)
dim(new_CCLE_tpm_match)
dim(new_getBM)


class(new_CCLE_demeter_match)
typeof(new_CCLE_demeter_match)

class(new_CCLE_sample_info_match)
typeof(new_CCLE_sample_info_match)

class(new_CCLE_tpm_gn_match)
typeof(new_CCLE_tpm_gn_match)

class(new_CCLE_tpm_match)
typeof(new_CCLE_tpm_match)

class(new_getBM)
typeof(new_getBM)

