# predictBiomarkers_mut

# if (!(names(interactions_dataframe)[1] == "proteinA" & names(interactions_dataframe)[2] == 
#       "proteinB" & names(interactions_dataframe)[3] == "score")) {
#   cat("ERROR: \\nthe input dataframe should contain an header with the following names: proteinA, proteinB, score")
#   stop()
# }

predictBiomarkers_mut <- function(Essentiality, CLxMut, iGn, iCL, co_pMut_iCL = 0.1, restCL = TRUE, string = TRUE, demeterF = TRUE, specMut = NULL){
  require(limma)
  require(STRINGdb)
  require(scales)
  require(qvalue)

                                   # TESTING !!!!!!!!!!!!!!!!!!!!!!!
                                   # string <- FALSE
  
  if (length(iCL) == 0){
    # showNotification("ERROR. \n Please, select the Cell Lines.", type = "error",duration = 4)
    return(list(MagicCube_Mut = NULL, Fit_mut_list = NULL, nMut = NULL, LengthCL = NULL, qvalue_Gn = NULL))
  }
  
  # Check matrices
  clName <- colnames(Essentiality)
  clName <-  unlist(lapply(strsplit(clName,"_"), function(x) {x[1]}))
  
  if(!identical(clName, rownames(CLxMut))){
    clMatchedMaskInEssentiality <- clName %in% rownames(CLxMut)
    print("Not matched in Essentiality matrix")
    print(clName[!clMatchedMaskInEssentiality])

    clMatchedMaskInCLxMut <- rownames(CLxMut) %in% clName
    print("Not matched in CLxMUT matrix")
    print(rownames(CLxMut)[!clMatchedMaskInCLxMut])

    print(dim(Essentiality))
    print(dim(CLxMut))
    
    stop("Cell Lines of Essentiality and CLxMut don't match.")
  }
  
  # Get the name of mutations
  Genes_Mut <- colnames(CLxMut)
  
  # Discard mutations
  Freq_mut <- colSums(CLxMut[iCL, ,drop=FALSE])
  Min_mut <- round(co_pMut_iCL * length(iCL))
  Max_mut <- length(iCL) - Min_mut
  iM <- which((Freq_mut >= Min_mut) & (Freq_mut <= Max_mut))

  if (is.null(specMut) == FALSE){
    iM <- which(colnames(CLxMut) == specMut)
  }
  

  
  if (length(iM) == 0){
    return(list(MagicCube_Mut = NULL, Fit_mut_list = NULL, nMut = NULL, LengthCL = NULL, qvalue_Gn = NULL))
  }
  
  
  
  # Wz_Mut_iCL_Z <- Wilcoxon.z.matrix(ExprT = Essentiality[iGn, iCL, drop = F], GeneGO = CLxMut[iCL, iM])
  
  # print("colnames(CLxMut[, iM,drop=FALSE])")
  # print(colnames(CLxMut[, iM,drop=FALSE]))
  
  MagicCube_Mut <- array(0, dim = c(length(iGn), length(iM), 18),
                         dimnames = list(rownames(Essentiality[iGn,,drop = FALSE]),
                                         colnames(CLxMut[,iM,drop=FALSE]),
                                         c("deltaEs", "t", "P.Value", "lfdr", "adj.P.Val_by_Mut", "adj.P.Val_by_GeneEs", 
                                            "nDeath_Mut","Ess_Mut",  "nDeath_WT","Ess_WT",  "nDeath_Target","Ess_Target","Ess_NoTarget",
                                           "RestCL_deltaEs","RestCL_t", "RestCL_P.Value", "Same_Dir",
                                           "String")))
  
  # print("MagicCube_Mut")
  # print(MagicCube_Mut)
  
  Fit_mut_list <- vector(length = length(iM), mode = "list")
  names(Fit_mut_list) <- Genes_Mut[iM]
  
  
  #  rownames(MagicCube_Mut_iCL_t) <- rownames(Essentiality[iGn,,drop = FALSE])
  # colnames(MagicCube_Mut_iCL_t) <- colnames(CLxMut[, iM])
  nMut <- Freq_mut[iM]
  
  # print("nMut -----------------------------------")
  # print(nMut)
  LengthCL <- length(iCL)
  
  meanTimeIter <- 0
  
  # Added by STRING computation
  numExtraIters <- 280
  
  for(m in 1:length(iM)){ # length(iM) For each mutation
    
    tic();
    
    incProgress(1/(length(iM)+numExtraIters), detail = sprintf("mutation %s/%s - %s%% - ETA: < %s min", m, length(iM),round((m)*100/(length(iM)+numExtraIters)),round(meanTimeIter*(length(iM)+numExtraIters-m)/60)+1 ))
    
    
    mutation <- Genes_Mut[iM[m]]
                    
    
    Dmatrix <- cbind(1,CLxMut[iCL, which(colnames(CLxMut) == mutation),drop=FALSE])
    colnames(Dmatrix) <- c("Intercept", mutation)
    
    Cmatrix <- t(t(c(0,1)))
    

    rN <- rownames(Essentiality[iGn, iCL, drop = FALSE])
    
            # print("Dmatrix")
            # print(Dmatrix)
            
            # print("dim(Dmatrix)")
            # print(dim(Dmatrix))

            # print("is.fullrank(Dmatrix)")
            # print(is.fullrank(Dmatrix))
            
            # print("nonEstimable(Dmatrix)")
            # print(nonEstimable(Dmatrix))
            
    if (is.fullrank(Dmatrix)){
      Fit_mut <- lmFit(Essentiality[iGn, iCL, drop = FALSE], Dmatrix)
      Fit_mut <- contrasts.fit(Fit_mut, Cmatrix)
      Fit_mut <- eBayes(Fit_mut)
             
      if (length(rN) == 1){ # Recover the gene name
        rownames(Fit_mut$p.value) <- rN
      }
            
    topG <- topTable(Fit_mut, number = Inf, sort.by = "none")
    
    topGPV <- topG$P.Value
     
    }else{
      Fit_mut <- NULL
      topG <- NULL
      topG$t <- NULL
      topG$P.Value <- NULL
      topG$adj.P.Val <- NULL
      topG$logFC <- NULL
      topGPV <- NULL
      break
    }

    Fit_mut_list[[m]] <- Fit_mut
     
    # print(c("Gene number ", m))
    # print(length(topGPV))
    # plot(1:length(topGPV),topGPV)
    # print(sum(is.na(topGPV)))
    # print(sum(is.null(topGPV)))
    # print(min(topGPV))
    # print(max(topGPV))
    # print(topGPV)
    
    # topGPV <- c(0.2290552766,0.5678138935,0.2700193225,0.6600138017,0.6180285660,0.5544109248,0.4581489774,0.7597182720,0.4599225052,0.8482075914,0.3818982954,0.5409233274,0.4811629090,0.3057456085,0.2698376047,0.2351537367,0.7123851750,0.0003455198,0.8812605892,0.2691708497,0.0042766804,0.8317333576,0.1700258403,0.0261800412,0.5240776160,0.5488027898,0.1501750853,0.6446250274,0.9722731234,0.8400729598,0.6462370633,0.1590270360,0.0387390935,0.1117360863,0.6274212252,0.3250359560,0.0644062404,0.8207699812,0.5741894596,0.0385389209,0.7728699573,0.4324397990,0.6546156728,0.3967281022,0.0574962053,0.2242306593,0.0374135517,0.5983831831,0.7678098431,0.9564257355,0.1620341051,0.1499190533,0.0006596887,0.9119445251,0.7378339462)
    # topGPV <- c(0.164294142,0.456870534,0.833385710,0.897183161,0.431220553,0.858321161,0.353540721,0.809970194,0.337595781,0.441538336,0.533423716,0.011567745,0.097508447,0.116724903,0.113577925,0.401689529,0.120208645,0.249854555,0.025570421,0.079853289,0.505866500,0.653544378,0.221835696,0.213152468,0.939594524,0.747016402,0.409382057,0.406364070,0.507450709,0.698955167,0.350103863,0.348916556,0.129913424,0.818777932,0.642778935,0.247754852,0.555105015,0.513786659,0.845760202,0.583686314,0.354473510,0.016947982,0.396941843,0.003987585,0.877231490,0.782572658,0.927539757,0.913960137,0.001173287,0.446160834,0.501744346,0.915401736,0.100816430,0.311178428,0.863903772) # m = 3
    if (length(topGPV) > 1){
      
      # print("topGPV")
      # print(topGPV)
      
      tryCatch(
        {
          lfdrV <- lfdr(topGPV)
          # print(lfdrV)
        },
        error=function(error_message) {
          #message(error_message)
          print("Changing to pi0 = 1") # ??????????????????????????????????????????????????????
          lfdrV <<- lfdr(topGPV, pi0 = 1) # == lfdrV <- lfdr(topGPV, lambda = 0)
          # lfdrV <<- lfdr(topGPV, pi0.method="bootstrap", na.rm = TRUE)
                # print(topGPV)
                # print(lfdrV)
        }
      )
    }else{
      lfdrV <- topGPV # ??????????????????????????????????
    }
    
    # print(lfdrV)
    
    
    # topG <- data.frame(Gene = rownames(topG), topG)
    # View(topG)
    # print(m)
    # print(topG)
    # print(topG$logFC)
    
    MagicCube_Mut[ , m, "deltaEs"] <- topG$logFC
    MagicCube_Mut[ , m, "t"] <- topG$t
    MagicCube_Mut[ , m, "P.Value"] <- topG$P.Value
    MagicCube_Mut[ , m, "adj.P.Val_by_Mut"] <- topG$adj.P.Val
    MagicCube_Mut[ , m, "lfdr"] <- lfdrV
    
    # MagicCube_Mut[ , m, "Ranking"] <- rank(topG$P.Value) 
    
    # Adjusting pvalues by gene esential
    Pvs <- MagicCube_Mut[ , , "P.Value"]
    
    Pvs <- as.matrix(Pvs)
    # dim(MagicCube_Mut)
    # dim(Pvs)
    
    adj.P.Val_by_GeneE <- apply(Pvs, 1, function(pv){pvA <- p.adjust(pv, method = "BH"); return(pvA)})
    MagicCube_Mut[ , , "adj.P.Val_by_GeneEs"] <- t(adj.P.Val_by_GeneE)
    
    # Get number of Death_target
    aux <- as.matrix(Essentiality[iGn,iCL, drop=FALSE])
    aux <- data.frame(aux)
    iMut <- which(Dmatrix[,2] == 1)
                      

    Ess_Mut <- rowMedians(as.matrix(aux[, iMut]))
    names(Ess_Mut) <- rownames(aux)
      
    # print("length(iMut)")
    # print(length(iMut))
    
    if (length(iMut)==1){
      nDeath_Mut <- sum(aux[, iMut] < -2)
      nDeath_WT <- rowSums(aux[, -iMut] < -2)
      Ess_WT <- rowMedians(as.matrix(aux[, -iMut]))
      nDeath_Mut <- as.matrix(nDeath_Mut)
    }else{
      if (length(iMut)==(dim(aux)[2]-1)){
        nDeath_Mut <- rowSums(aux[, iMut] < -2)
        nDeath_WT <- sum(aux[, -iMut] < -2)    
        Ess_WT <- as.matrix(aux[, -iMut])
      }else{
        if (length(iMut)==(dim(aux)[2])){
          nDeath_Mut <- rowSums(aux[, iMut] < -2)
          nDeath_WT <- NULL    
          Ess_WT <- NULL
        }else{
          nDeath_Mut <- rowSums(aux[, iMut] < -2)
          nDeath_WT <- rowSums(aux[, -iMut] < -2)    
          Ess_WT <- rowMedians(as.matrix(aux[, -iMut]))
        }
        
      }  
    }
    
    
    # print("iMut")
    # print(iMut)
    # 
    # print("dim(aux)")
    # print(dim(aux))
    # 
    # print("Ess_WT")
    # print(Ess_WT)
    # 
    # dim("Ess_WT")
    # dim(Ess_WT)
    # 
    # print("is.null(Ess_WT)")
    # print(is.null(Ess_WT))
    
    if (is.null(Ess_WT) == FALSE){
      names(Ess_WT) <- rownames(aux)
    }
    
    MagicCube_Mut[ , m, "Ess_Mut"] <- Ess_Mut
    MagicCube_Mut[ , m, "nDeath_Mut"] <- nDeath_Mut
    MagicCube_Mut[ , m, "Ess_WT"] <- Ess_WT
    MagicCube_Mut[ , m, "nDeath_WT"] <- nDeath_WT
    MagicCube_Mut[ , m, "Ess_Target"] <- rowMins(cbind(Ess_Mut, Ess_WT))
    MagicCube_Mut[ , m, "Ess_NoTarget"] <- rowMaxs(cbind(Ess_Mut, Ess_WT))
    
                            # print("Ess_Target printed #1")
                            # print(MagicCube_Mut[ , 1, "Ess_Target"][1:4])
                            # print(MagicCube_Mut[ , 1, "Ess_NoTarget"][1:4])
                            # stop("bye inside predictBiomarkers_mut.R")
                  
                          
    whichMin <- apply(cbind(Ess_Mut, Ess_WT), 1, function(x){
      if (is.na(x[1]) & is.na(x[2])) {
        NA
      }else{      
        which.min(x)
      }
      })
    
    # length(nDeath_Mut)
    # length(nDeath_WT)
    # length(whichMin)
    # class(nDeath_Mut)
    # class(nDeath_WT)
    # class(whichMin)
     
    aux2 <- data.frame(nDeath_Mut, nDeath_WT, whichMin)
    
    nDeath_Target <- apply(aux2, 1, function(x){x[x[3]]})
    MagicCube_Mut[ , m, "nDeath_Target"] <- nDeath_Target
    
    
    if(restCL) {
                # print(dim(CLxMut))
                # print(mutation)
      
        Dmatrix_rest <- cbind(1,CLxMut[-iCL, which(colnames(CLxMut) == mutation),drop=FALSE])
        colnames(Dmatrix_rest) <- c("Intercept", mutation)
        if(sum(Dmatrix_rest[,2]) == 0) next()
          
                  # print(dim(Essentiality[iGn, -iCL,drop = FALSE]))
                  # print(dim(Dmatrix_rest))
                  # stop("halllt")
                  
        # with the rest of CL
        rN <- rownames(Essentiality[iGn, -iCL, drop = FALSE])
        
        Fit_mut_rest<-lmFit(Essentiality[iGn, -iCL, drop = FALSE], Dmatrix_rest)
        Fit_mut_rest<-contrasts.fit(Fit_mut_rest, Cmatrix)
        Fit_mut_rest <- eBayes(Fit_mut_rest)
        
        if (length(rN) == 1){ # Recover the gene name
          rownames(Fit_mut$p.value) <- rN
        }
        
        topG_rest <- topTable(Fit_mut_rest, number = Inf, sort.by = "none")
        
        # topG <- data.frame(Gene = rownames(topG), topG)
        # View(topG)
        MagicCube_Mut[ , m, "RestCL_t"] <- topG_rest$t
        MagicCube_Mut[ , m, "RestCL_P.Value"] <- topG_rest$P.Value
        # MagicCube_Mut[ , m, "RestCL_Ranking"] <- rank(topG_rest$P.Value) 
        MagicCube_Mut[ , m, "RestCL_deltaEs"] <- topG_rest$logFC
        MagicCube_Mut[ , m, "Same_Dir"] <- ((topG$t * topG_rest$t) > 0) * 1
        
      }
      
      endIt <- toc(quiet = TRUE);
      if (m == 1){
        meanTimeIter <- endIt$toc - endIt$tic;# In seconds
      }else{
        meanTimeIter <-  (meanTimeIter + (endIt$toc - endIt$tic))/2; # In seconds
      }
    
  }
  
  # print("mean time iter is: ")
  # print(meanTimeIter)
  
  
  for(iS in 1:70){ # 1/4
    incProgress(1/(length(iM)+numExtraIters), detail = sprintf("computing string relationship - %s%% - ETA: < %s min",round((m+iS)*100/(length(iM)+numExtraIters)),round(meanTimeIter*(length(iM)+numExtraIters-m+iS)/60)+1  ))
  }

  
  tic();
  ## String interactions ----------------------------------------------------------------------------------------------
  if(string){
    #(9606 for Human, 10090 for mouse).
    # string_db <- STRINGdb$new(version="10", species=9606,score_threshold=0, input_directory="") # Original code
    
    string_db <- STRINGdb$new(version="11", species=9606,score_threshold=0, input_directory="./dataShiny/stringFiles/")
    
    string_proteins <- string_db$get_proteins()
    
    for(iS in 71:140){ # 2/4
      incProgress(1/(length(iM)+numExtraIters), detail = sprintf("computing string relationship - %s%% - ETA: < %s min",round((m+iS)*100/(length(iM)+numExtraIters)),round(meanTimeIter*(length(iM)+numExtraIters-m+iS)/60)+1  ))
    }
  
    gns <- unique(c(rownames(Essentiality[iGn,,drop = FALSE]), colnames(CLxMut[, iM,drop=FALSE])))
    ids <- string_proteins$protein_external_id[string_proteins$preferred_name %in% gns]
    
    Genes_not_mapped <- gns[!(gns %in% string_proteins$preferred_name)]
    
    # cat(sprintf("\n%s genes mapped in String (%s out of %s) \n", percent(length(ids) / length(gns)), length(ids), length(gns)))
    # cat("\nGenes not mapped in String: ", Genes_not_mapped)
    
    string_db_interactions <-  string_db$get_interactions(ids) # This consumes a large amount of time!
    
    for(iS in 141:210){ # 3/4
      incProgress(1/(length(iM)+numExtraIters), detail = sprintf("computing string relationship - %s%% - ETA: < %s min",round((m+iS)*100/(length(iM)+numExtraIters)),round(meanTimeIter*(length(iM)+numExtraIters-m+iS)/60)+1  ))
    }
    
    interactions <- string_db_interactions
    interactions$from <- string_proteins$preferred_name[match(interactions$from, string_proteins$protein_external_id)]
    interactions$to <- string_proteins$preferred_name[match(interactions$to, string_proteins$protein_external_id)]
    
    # interactions <- interactions[, c("from", "to", "coexpression_transferred", "experiments", "database")]
    # interactions <- interactions[which(rowSums(interactions[, -c(1,2)]) > 0), ]
    
    interactions <- interactions[, c("from", "to", "combined_score")]
    interactions <- unique(interactions)
    
    
    
    interactions_rev <- interactions
    colnames(interactions_rev) <- colnames(interactions)[c(2,1,3:ncol(interactions))]
    interactions <- rbind(interactions, interactions_rev)
    rm(interactions_rev)
    
    GeneA <- as.factor(interactions$from)
    GeneB <- as.factor(interactions$to)
    AxB <- sparseMatrix(as.numeric(GeneA), as.numeric(GeneB),x=1)
    rownames(AxB) <- levels(GeneA)
    colnames(AxB) <- levels(GeneB)
    dim(AxB)
    
    Nmap <- setdiff(gns, colnames(AxB))
    aux1 <- matrix(0, ncol = length(Nmap), nrow = nrow(AxB))
    colnames(aux1) <- Nmap
    rownames(aux1) <- rownames(AxB)
    
    AxB <- cbind(AxB, aux1)
    
    aux2 <- matrix(0, ncol = ncol(AxB), nrow = length(Nmap))
    colnames(aux2) <- colnames(AxB)
    rownames(aux2) <- Nmap
    
    AxB <- rbind(AxB, aux2)
    
    AxB <- as.matrix(AxB)
    
    diag(AxB) <- 1
    
                                # print(dim(AxB))
                                # print(dim(MagicCube_Mut))
    iR <- match(rownames(MagicCube_Mut), rownames(AxB))
    iC <- match(colnames(MagicCube_Mut), colnames(AxB))
    
    AxB_m <- AxB[iR, iC]
        
                                # print(AxB_m)
                                # print(length(AxB_m))
                                # print(dim(AxB_m))
    
    if (length(AxB_m) > 0) {
      MagicCube_Mut[, ,"String"] <- AxB_m
    }
    
    # table(AxB[,"EP300"]) 
    # 0   1 
    # 417  63
    
    # View(cbind(rownames(AxB), AxB[, "EP300", drop = F]))
    # cat(names(which(AxB[,"EP300"]==1)), sep="\n")
    # cat(interactions$to[which((interactions$combined_score > 500) & (interactions$from == "EP300"))], sep = "\n")
  }
  
  endString <-  toc(quiet = TRUE);
  timeString <- endString$toc - endString$tic;# In seconds
  # print("time string is: ")
  # print(timeString)
  # print(paste("time string is ", round(timeString/meanTimeIter)," times meanTimeIter"))
  

  for(iS in 211:numExtraIters){ # 3/4
    incProgress(1/(length(iM)+numExtraIters), detail = sprintf("computing string relationship - %s%% - ETA: < %s min",round((m+iS)*100/(length(iM)+numExtraIters)),round(meanTimeIter*(length(iM)+numExtraIters-m+iS)/60)+1  ))
  }  
  
  
  if (length(topG$P.Value) > 1){
    tryCatch(
      {
        qvG <- qvalue(topG$P.Value)
        # print(qvG)
      },
      error=function(error_message) {
        # message(warning_message)
        print("Changing to pi0 = 1") # ??????????????????????????????????????????????????????
        qvG <<- qvalue(topG$P.Value, pi0 = 1) # == qvG <- qvalue(topG$P.Value, lambda = 0)
      }
    )
  }else{
    qvG <- topG$P.Value 
  }


  return(list(MagicCube_Mut = MagicCube_Mut, Fit_mut_list = Fit_mut_list, nMut = nMut, LengthCL = LengthCL, qvalue_Gn = qvG))
  
}
