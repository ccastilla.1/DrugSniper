## ==================================================================================== ##
# MutationAchilles Shiny App for predicting transcript biomarkers.
# Copyright (C) 2019 Fernando Carazo & Carlos Castilla
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# You may contact the authors of this code, Fernando Carazo, at <fcarazo@tecnun.es>, and Carlos Castilla, at <ccastilla.1@tecnun.es>
## ==================================================================================== ##



tabPanel(title = "2. Predict Biomarkers for a Target Gene",
         
         sidebarLayout(
         # Sidebar
         sidebarPanel(width = 3,
                      h3("Your Analysis"),
                      h5("Primary sites selected:"),
                      h6(textOutput("O_txtout3")),
                      h5("Number of samples:"),
                      h6(textOutput("O_nCL_sel3")),
                      h5("Number of Protein Targets:"),
                      h6(textOutput("O_txtout_nEsGn3")),
                      #br(),
                      #br(),
                      hr(style="border-color: black; background-color: black;color: black;"),
                      h3("Personalized treatment for"),
                      radioButtons(inputId = "I_ALL_PROTEIN_DRUG", label = "Select:", 
                                   choiceValues = list("One or more Protein Targets","One or more Drugs","One or more Mutations","All Protein Targets & Drugs & Mutations"), 
                                   choiceNames =  list("One or more Protein Targets","One or more Drugs","One or more Mutations","All Protein Targets & Drugs & Mutations"), 
                                   selected = "All Protein Targets & Drugs & Mutations"),
                      
                      # Inputs ------------------------------------------------------------------
                      div(id = "sp1",
                      # TARGETS/GENES ---------------------------------
                      # Select all targets/genes (Minutes)
                      # Select One or Multiple targets/genes (Seconds)
                      uiOutput("O_GeneE_to_predict_bmkr"), 
                      checkboxInput(inputId = "I_All_GeneE_to_predict_bmkr",
                                    label = "Select all targets/genes (Minutes)",
                                    value = FALSE)
                      ),
                      div(id = "sp2",
                      # DRUGS -----------------------------------------
                      # Select all drugs (Minutes)
                      # Select One or Multiple drugs (Seconds)
                      uiOutput("O_Drug_to_predict_bmkr"),
                      checkboxInput(inputId = "I_All_Drug_to_predict_bmkr",
                                    label = "Select all drugs (Minutes)",
                                    value = FALSE)
                      ),
                      div(id = "sp3",
                          # Bmkrs -----------------------------------------
                          # Select all bmkrs 
                          # Select One or Multiple bmkrs 
                          # uiOutput("O_Bmkr_to_predict_bmkr"),
                          selectizeInput(inputId = "I_Bmkr_to_predict_bmkr", width = '100%', label = "Select One or Multiple Mutations (type & wait):", choices = NULL, multiple = TRUE, options = list(maxOptions = 15)), # 
                          checkboxInput(inputId = "I_All_Bmkr_to_predict_bmkr",
                                        label = "Select all mutations",
                                        value = FALSE)
                      ),
                      
                      
                      fluidRow(
                                column(width = 2, align="center", offset = 1,
                                  div(id = "uc1", img(src="uc1.jpg", width = "45px"))
                                 ),
                                column(width = 2, align="center", offset = 1,
                                       div(id = "uc2", img(src="uc2.jpg", width = "45px"))
                                 ),
                                column(width = 2, align="center", offset = 1,
                                       div(id = "uc3", img(src="uc3.jpg", width = "45px"))
                                 ),
                               ),
                      br(),
                      actionButton("I_aB_predictBmrk", "Go!", width = '100%',heigth = '200%', 
                                   style="color: #ffffff; background-color: #77933C; border-color: #216b2c"), # Look for text redistribution inside the actionButton !!!!!!!!!!!!!
                      tags$head(tags$style(type = "text/css",".btn-default { background-image: none;}")),
                      tags$head(tags$style(type = "text/css",".btn-default:hover { background-image: none;}")),
                      
                      # h5("(Takes a few seconds)"),
                      # br(),
                      # br(),
                      # br(),
                      
                      
                      h3("Biomarker parameters"),
                      # uiOutput("O_renderUI_I_co_demeter_slider_s"),
                      # sliderInput(inputId = "I_qntl_exp_tr", "Quantile filter of transcripts", min = 0, max = 1, value = 0.5),
                      # % of mutation slider
                      sliderInput(inputId = "I_perc_mut", "% Mutation", min = 0, max = 1, value = 0.2) %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Min. % (minP) of cell lines with mutation biomarker",
                               content = "<center>Each mutation biomarker will be present in p % of the cell lines analyzed. This % p will be over minP and below (1-minP).</center>"
                        ),
                      
                      # Select rest of CLs 
                      checkboxInput(inputId = "I_restCL_to_predict_bmkr",
                                    label = "Compare with the rest of cell lines",
                                    value = FALSE) %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Compare results of selected cell lines against results from the rest of cell lines",
                               content = "<center>Results from the rest of cell lines (cell lines not selected in the analysis) will be computed, so comparison with the results of selected cell lines is possible.</center>"
                        ),
                      
                      h3("Essentiality parameters"),
                      h4("Essentiality filters"),
                      uiOutput("O_renderUI_I_co_demeter_slider") %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Essentiality score threshold",
                               content = "<center>Genes with essentiality score under this threshold value will be considered as essential.</center>"
                        ),
                      sliderInput(inputId = "I_co_pess_iCL", label="Minimal percentage of essentiality (Selected CLs)", min = 0, max = 1, value = 0.20) %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Min. % of cell lines for which a gene has to be essential",
                               content = "<center>Minimal % of selected cell lines for which a gene has to be essential in order to consider it as essential and include it in the analysis.</center>"
                        ),
                      sliderInput(inputId = "I_co_enr", label="Enrichment of essentiality in CLs (#Observed / #Expected)", min = 0, max = 10, value = 1,step = 0.1) %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Enrichment ratio",
                               content = "<center>Value obtained from: (Number of cell lines in which a gene is essential)/(Number of cell lines in which a gene will be expected to be essential) == (#Observed / #Expected). It indicates if a gene is essential in more (>1), equal(==1) or fewer(<1) cell lines than it will be expected.</center>"
                        ),
                      br(),
                      # br(),
                      h4("Expression filters"),
                      sliderInput(inputId = "I_co_exp", label="Minimal required expression of essential genes (TPM)", min = 0, max = 20, value = 1) %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Minimal required expression in TPM of essential genes",
                               content = "<center>Minimal Transcripts Per Million (TPM) required for a gene to be included in the analysis.</center>"
                        ),
                      sliderInput(inputId = "I_qntl_exp", label="Percentage of CLs that pass the expression filter", min = 0, max = 1, value = 0.75) %>%
                        helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Min. % of cell lihnes for which a gene has expression",
                               content = "<center>Minimal % of cell lines in which a gene has to have expression >= 1 TPM.</center>"
                        ),
                      
                      
                      actionButton("I_reset_params_all_fb", "All parameters to default values", width = '100%')
                      
         ),
         # Main panel
         mainPanel(# co_FDR = 0.4, co_log2FC = 2
           # tabsetPanel(
             tabPanel("Ranking of biomarkers",
                      h3("Number of biomarkers:"),
                      h4(textOutput("O_n_bmrks")),
                      h3("a) Ranking of biomarkers of selected tumor:"),
                      fluidRow(column(width = 1, h4("Filters:")), column(width = 2, numericInput("I_co_PV_tr", "P-value<", min = 0, value =  0.05, max = 0.1, step = 0.001) %>%
                                 helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Min. P-value for a pair to be listed",
                                        content = "<center>P-Value threshold for showing pairs (essential gene-mutation) in the results table.</center>"
                                 )),
                               #column(width = 2, numericInput("I_co_logFC_tr", "|Log2FC| > ...", min = .5, max = 10, value =  1.5, step = .5)),
                               uiOutput("O_co_DeltaEs_mut"),
                               column(width = 2, numericInput("I_co_lfdr_tr", "Local FDR<", min = 0, max = 0.95, value =  0.7,  step = 0.1) %>%
                                 helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Min. Local FDR for a pair to be listed",
                                        content = "<center>Local False Discovery Rate (LFDR) threshold for showing pairs (essential gene-mutation) in the results table.</center>"
                                 )), # 0.2
                      #          ),
                      # fluidRow(
                               # Select use STRING
                               column(width = 2, checkboxInput(inputId = "I_STRING_to_predict_bmkr",
                                             label = "Filter by STRING relationship",
                                             value = TRUE) %>%
                                 helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Filter results by STRING relationship",
                                        content = "<center>Only pairs (essential gene-mutation) biologically related according to the STRING database will be listed.</center>"
                                 )),
                               column(width = 2,checkboxInput(inputId = "I_targets_with_drugs_2", 
                                      label = "Only targets with drugs",
                                      value = FALSE) %>%
                                 helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Filter results with drugs",
                                        content = "<center>Only pairs (essential gene-mutation) with at least one drug (associated to the protein target) will be listed.</center>"
                                 )),
                               # column(width = 3,checkboxInput(inputId = "I_fda_drugs", 
                               #                                label = "Show only FDA approved drugs",
                               #                                value = FALSE) %>%
                               # helper(buttonLabel = "Ok", size = "m", type = "inline", fade = FALSE, title = "Filter results with FDA approved drugs",
                               #        content = "<center>Only pairs (essential gene-mutation) with at least one FDA approved drug (associated to the protein target) will be listed.</center>"
                               # ))
                               ),
                      br(),
                      div(id = "mp2",
                        # br(),
                        div(withSpinner(DT::dataTableOutput(outputId = "O_rT_Pairs_tr")), style = "font-size: 95%; width: 100%"),
                        br(),
                        downloadButton("O_selected_bmrk_download","Download table"),
                        br(),
                        h3("b) Plot biomarker (select a row):"),
                        div(id = "mp3",
                          # fluidRow(
                          #   # column(width = 6, withSpinner(plotOutput("O_gg_bmrk_1"))), 
                          #   # column(width = 6,withSpinner(plotOutput("O_gg_bmrk_2")))
                          #   
                          #   # Volcano plot of mutations
                          #   column(width = 6, withSpinner(plotOutput("O_gg_bmrk_6"))), 
                          #   
                          #   # Boxplot mutations of gene pairs
                          #   column(width = 6,withSpinner(plotOutput("O_gg_bmrk_7")))
                          #   
                          #   ),
                          fluidRow( # Volcano plot of mutations
                            column(width = 6, withSpinner(plotOutput("O_gg_bmrk_6", height="500px"))), 
                           # Boxplot mutations of gene pairs
                            column(width = 6,withSpinner(plotOutput("O_gg_bmrk_7", height="500px")))
                          ),
                          # fluidRow(
                            # column(width = 6, withSpinner(plotOutput("O_gg_bmrk_5"))),
                            # column(width = 6, withSpinner(plotOutput("O_gg_bmrk_3")))),                      
                          downloadButton("plotA2_p","Download plot")
                        )
                      )
             # )#,
             
             # tabPanel("P-value density histogram",
             #          br(),
             #          fluidRow(column(width = 6, h3("Genes"), withSpinner(plotOutput("O_gg_qvalue_gn"))) ),
             #                   # column(width = 6, h3("Transcripts"),withSpinner(plotOutput("O_gg_qvalue_tr")))),
             #          downloadButton("plot_qvalue_p","Download plot")
             #          )
             
           )
         )
         )
)


