
# Prepare GDSCTOOLS matrices -----------------------------------------------------------------------------
gc()
cat("\f")
# Clean all object from workspace
rm(list = ls())

library(rstudioapi) # Include it in helpers.R
setwd(dirname(getActiveDocumentContext()$path)) # Change into file directory

library(xml2)
library(XML)



alreadyComputed <- 1

if (alreadyComputed == 0){
  cell_line_details_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/cellosaurus.xml"
  cell_line_details <- read_xml(cell_line_details_fn)
  
  # dataAll <- xmlParse(cell_line_details)
  # xml_data_all <- xmlToList(dataAll)
  
  clCellosaurus <- xml_find_all(cell_line_details, ".//cell-line")
  
  
  
  cellosaurusLines <- data.frame(sex="", age="", accession="", name="", disease="", xref_database="", xref_category="", xref_accession="", stringsAsFactors = FALSE)
  nCL <- length(clCellosaurus) # 118785
  for(i in 1:nCL){
    
    print(paste0(as.character(i)," out of ",as.character(nCL), " --> ", as.character(round(i*100/nCL, digits = 1)), "% completed") )
    
    currCL <- clCellosaurus[[i]]
    dataCL <- xmlParse(currCL)
    xml_data_cl <- xmlToList(dataCL)
    
    # Sex
    cl_sex <- as.character(xml_data_cl$.attrs["sex"])
    
    # Age
    cl_age <- as.character(xml_data_cl$.attrs["age"])
    
    # Accession
    nAL <- length(xml_data_cl$`accession-list`)
    cl_accession <- NULL
    for(j in 1:nAL){
      cl_accession <- c(cl_accession, as.character(xml_data_cl$`accession-list`[j]$accession$text))
    }
    
    # Name
    nNL <- length(xml_data_cl$`name-list`)
    cl_name <- NULL
    for(k in 1:nNL){
      cl_name <- c(cl_name, as.character(xml_data_cl$`name-list`[k]$name$text))
    }
    
    # Disease
    nDL <- length(xml_data_cl$`disease-list`)
    cl_disease <- NULL
    for(l in 1:nDL){
      cl_disease <- c(cl_disease, as.character(xml_data_cl$`disease-list`[l]$`cv-term`$text))
    }
    
    # Xref
    nXL <- length(xml_data_cl$`xref-list`)
    cl_xref_database <- NULL
    cl_xref_category <- NULL
    cl_xref_accession <- NULL
    for(m in 1:nXL){
      cl_xref_database <- c(cl_xref_database, as.character(xml_data_cl$`xref-list`[m]$xref$.attrs["database"]))
      cl_xref_category <- c(cl_xref_category, as.character(xml_data_cl$`xref-list`[m]$xref$.attrs["category"]))
      cl_xref_accession <- c(cl_xref_accession, as.character(xml_data_cl$`xref-list`[m]$xref$.attrs["accession"]))
    }
  
    newLine <- c(cl_sex, cl_age, list(cl_accession), list(cl_name), list(cl_disease), list(cl_xref_database),list(cl_xref_category), list(cl_xref_accession))
    names(newLine) <- c("sex","age", "accession", "name", "disease", "xref_database", "xref_category", "xref_accession")
    newLine <- t(newLine)
    newLine <- as.data.frame(newLine)
    cellosaurusLines <- rbind(cellosaurusLines, newLine)
  }
  cellosaurusLines <- cellosaurusLines[-1,]
  
  
  save(cellosaurusLines, file = "./cellosaurusLines.RData")

} # end of --if (alreadyComputed == 0){--



load("./cellosaurusLines.RData")

allAccessions <- cellosaurusLines$accession
allNames <- cellosaurusLines$name
allDiseases <- cellosaurusLines$disease
allXrefDatabases <- cellosaurusLines$xref_database
allXrefAccessions <- cellosaurusLines$xref_accession


# Testing
accesssionInd <- grep("CVCL_1397", allAccessions, ignore.case = TRUE) # 80766
accesssionInd

nameInd <- grep("NCIH1836", allNames, ignore.case = TRUE)
nameInd

accesssionInd <- grep("CORL88", allAccessions, ignore.case = TRUE)
accesssionInd

nameInd <- grep("CORL88", allNames, ignore.case = TRUE)
nameInd

# SCLC
diseaseInd <- grep("small cell lung cancer", allDiseases, ignore.case = TRUE)
diseaseInd




# COSMIC to DEPMAP id table -----------------------------------------------------------------------
xrefInd <- grep("cosmic", allXrefDatabases, ignore.case = TRUE)
xrefInd[1:10]

# 683665 or 907041
# xrefNameCosmicInd <- grep("907041", allXrefAccessions, ignore.case = TRUE)
# xrefNameCosmicInd
# databases <- unlist(allXrefDatabases[xrefNameCosmicInd])
# indCOSMIC <- grep("cosmic", databases, ignore.case = TRUE)
# indDepMap <- grep("DepMap", databases, ignore.case = TRUE)
# accessions <- unlist(allXrefAccessions[xrefNameCosmicInd])
# accessions[indCOSMIC]
# accessions[indDepMap]


cosmic2depmap <- matrix(data=NA, nrow = 1,ncol = 2)
colnames(cosmic2depmap) <- c("cosmicID", "depmapID")
for (i in 1:length(xrefInd)){
  databases <- unlist(allXrefDatabases[xrefInd[i]])
  indCOSMIC <- grep("Cosmic", databases, ignore.case = TRUE)
  indDepMap <- grep("DepMap", databases, ignore.case = TRUE)
  
  if (length(indCOSMIC) == 0){
    next
  }
  if (length(indDepMap) == 0){
    next
  }
  accessions <- unlist(allXrefAccessions[xrefInd[i]])

  for (j in 1:length(indCOSMIC)){
    cosmicID <- as.character(accessions[indCOSMIC[j]])
    depmapID <- as.character(accessions[indDepMap[1]])
    newLine <- c(cosmicID, depmapID) 
    # print(newLine)
    cosmic2depmap <- rbind(cosmic2depmap, newLine)
  }
}
cosmic2depmap <- cosmic2depmap[-1,]
rownames(cosmic2depmap) <- cosmic2depmap[, "cosmicID"]


stop("bye bye")


load("./cellosaurusLines.RData")


# Test if we can translate all COSMIC IDs with cellosaurus info ------------------------------------------------------------------
# Genomic features matrix -------------------------------------
genomic_features_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/genomic_features_v17.csv"
genomic_features <- read.table(file = genomic_features_fn, sep = ',', header = TRUE)
cl_names_cosmic <- as.character(genomic_features$COSMIC_ID)


for (i in 1:length(cl_names_cosmic)){
  print(i)
  print(cl_names_cosmic[i])
  cl_translation <- cosmic2depmap[cl_names_cosmic[i],"depmapID"] #
  print(cl_translation)
}

cl_translation <- cosmic2depmap[cl_names_cosmic,"depmapID"] # Ok
sum(is.na(cl_translation)) # 0

genomic_features$COSMIC_ID <- cl_translation
sum(is.na(genomic_features$COSMIC_ID)) # 0

COSMIC_ID <- genomic_features$COSMIC_ID
COSMIC_ID <-  unlist(lapply(strsplit(COSMIC_ID,"\\-"), function(x) {x[2] }))
genomic_features$COSMIC_ID <- COSMIC_ID

# write.table(genomic_features, file='C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/genomic_features_v17_mod.tsv', quote=FALSE, sep='\t')
write.csv(genomic_features, file='C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/genomic_features_v17_mod.csv', row.names = FALSE)


genomic_features_new_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/genomic_features_v17_mod.tsv"
genomic_features_new <- read.table(file = genomic_features_new_fn, sep = '\t', header = TRUE)


# Essentiality matrix -----------------------------------------
essentiality_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/scaledBayesianFactors.tsv"
essentiality <- read.table(file = essentiality_fn, sep = '\t', header = TRUE)

cl_names_depmap <- colnames(essentiality)
cl_names_depmap <-  unlist(lapply(strsplit(cl_names_depmap,"\\."), function(x) {paste0(x[1], "-", x[2]) }))
colnames(essentiality) <- cl_names_depmap

essentiality <- t(essentiality)
COSMIC_ID <- rownames(essentiality)

COSMIC_ID <-  unlist(lapply(strsplit(COSMIC_ID,"\\-"), function(x) {x[2] }))


essentiality <- cbind(COSMIC_ID ,essentiality)
colnames(essentiality) <- essentiality[1, ]
rownames(essentiality) <- NULL
essentiality <- essentiality[-1, ]
colnames(essentiality)[1] <- "COSMIC_ID"

genes <- colnames(essentiality)
for (i in 2:length(genes)){
  genes[i] <- paste0("Drug_",as.character(i-1),"_",genes[i])
}
colnames(essentiality) <- genes
  
write.csv(essentiality, file='C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/scaledBayesianFactors.csv', row.names = FALSE)


dim(essentiality) # 489 17344 --> 489 cell lines & 17343 genes
colnames(essentiality)[1:5] # "COSMIC_ID"      "Drug_1_SHOC2"   "Drug_2_NDUFA12" "Drug_3_SDAD1"   "Drug_4_FAM98A" 

dim(genomic_features) # 988 680 --> 988 cell lines & 678 bmkrs
colnames(genomic_features)[1:5] # "COSMIC_ID"     "TISSUE_FACTOR" "MSI_FACTOR"    "ABCB1_mut"     "ABL2_mut"  

# 204 cosmic identifiers in your IC50 (essentiality) could not be found in the genomic feature matrix (genomic_features). They will be dropped.
# 489 - 204 = 285 cell lines in essentiality that have correspondence in the genomic_features matrix



# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# binaryDepScores.tsv
binaryDepScores_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_binaryDepScores_190724.tsv/binaryDepScores.tsv"
binaryDepScores <- read.table(file = binaryDepScores_fn, sep = '\t', header = TRUE)


# BayesianFactors.tsv
BayesianFactors_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/BayesianFactors.tsv"
BayesianFactors <- read.table(file = BayesianFactors_fn, sep = '\t', header = TRUE)


# binaryDepScores_fn.tsv
binaryDepScores_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/binaryDepScores.tsv"
binaryDepScores <- read.table(file = binaryDepScores_fn, sep = '\t', header = TRUE)


# corrected_logFCs.tsv
corrected_logFCs_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/corrected_logFCs.tsv"
corrected_logFCs <- read.table(file = corrected_logFCs_fn, sep = '\t', header = TRUE)


# logFCs.tsv
logFCs_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/logFCs.tsv"
logFCs <- read.table(file = logFCs_fn, sep = '\t', header = TRUE)


# MageckFDRs.tsv
MageckFDRs_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/MageckFDRs.tsv"
MageckFDRs <- read.table(file = MageckFDRs_fn, sep = '\t', header = TRUE)


# qnorm_corrected_logFCs.tsv
qnorm_corrected_logFCs_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/qnorm_corrected_logFCs.tsv"
qnorm_corrected_logFCs <- read.table(file = qnorm_corrected_logFCs_fn, sep = '\t', header = TRUE)

# scaledBayesianFactors.tsv
scaledBayesianFactors_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/scaledBayesianFactors.tsv"
scaledBayesianFactors <- read.table(file = scaledBayesianFactors_fn, sep = '\t', header = TRUE)
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------




genomic_features_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/genomic_features_v17.csv"
genomic_features <- read.table(file = genomic_features_fn, sep = ',', header = TRUE)

cell_line_details_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/Cell_Lines_Details.xlsx"
cell_line_details <- as.data.frame(readxl::read_xlsx(cell_line_details_fn))

cell_line_details$`Sample Name` <- as.character(cell_line_details$`Sample Name`)
cell_line_details$`COSMIC identifier` <- as.character(cell_line_details$`COSMIC identifier`)

cell_line_details <- cell_line_details[,c("Sample Name", "COSMIC identifier")]
cell_line_details <- cell_line_details[!is.na(cell_line_details$`COSMIC identifier`),]



rownames(cell_line_details) <- cell_line_details$`COSMIC identifier`
sum(is.na(cl_translation)) # 0


library(stringr)
sample_name <- cell_line_details$`Sample Name`
# sample_name <- str_replace_all(sample_name, "\\-", "")
# sample_name <- toupper(sample_name)
# cell_line_details$`Sample Name` <- sample_name

duplicatedMask <- !duplicated(cell_line_details$`Sample Name`)
cell_line_details <- cell_line_details[duplicatedMask,]
rownames(cell_line_details) <- cell_line_details$`COSMIC identifier`


cl_names_cosmic <- as.character(genomic_features$COSMIC_ID)
cl_translation <- cell_line_details[cl_names_cosmic,"Sample Name"] # Contains NA so it is not complete
sum(is.na(cl_translation))

isnaMask <- !is.na(cl_translation)
genomic_features <- genomic_features[isnaMask,]
cl_translation <- cl_translation[isnaMask]

duplicatedMask <- !duplicated(cl_translation)
genomic_features <- genomic_features[duplicatedMask,]

rownames(genomic_features) <- cl_translation[duplicatedMask]



# cell_line_details <- cell_line_details[!duplicated(cl_translation),]
# rownames(cell_line_details) <- cl_translation[!duplicated(cl_translation)]


essentiality_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/broad_essentiality_matrices_190724/scaledBayesianFactors.tsv"
essentiality <- read.table(file = essentiality_fn, sep = '\t', header = TRUE)
rownames(essentiality) <- essentiality[,1]
essentiality <- essentiality[,-1]

cl_names_depmap <- colnames(essentiality)
cl_names_depmap <-  unlist(lapply(strsplit(cl_names_depmap,"\\."), function(x) {paste0(x[1], "-", x[2]) }))
colnames(essentiality) <- cl_names_depmap


sample_info_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/depmap/sample_info_20Q2.csv"
sample_info <- read.csv(sample_info_fn, check.names=FALSE)

# rownames(sample_info) <- sample_info$DepMap_ID
# cl_translation <- sample_info[cl_names_depmap,"COSMICID"] # Contains NA so it is not complete

sample_info$DepMap_ID <- as.character(sample_info$DepMap_ID)
sample_info$COSMICID <- as.character(sample_info$COSMICID)
sample_info$stripped_cell_line_name <- as.character(sample_info$stripped_cell_line_name)

sample_info <- sample_info[,c("DepMap_ID", "COSMICID","stripped_cell_line_name")]



sample_info <- sample_info[!is.na(sample_info$stripped_cell_line_name),]
rownames(sample_info) <- sample_info$DepMap_ID

cl_names_depmap <- colnames(essentiality)
cl_translation <- sample_info[cl_names_depmap,"stripped_cell_line_name"]
sum(is.na(cl_translation)) # 0

# colnames(essentiality) <- cl_translation
# cl_essentiality <- colnames(essentiality)
# genomic_features[cl_essentiality,"TISSUE_FACTOR"]


# cl_translation <-  unlist(lapply(strsplit(cl_translation,"\\."), function(x) {paste0(x[1], "-", x[2]) }))




# -----------------------------------------------------------------------------------------------------------------------------------------------
# Test other table ...
cell_line_annotations_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/depmap/Cell_lines_annotations_20181226.txt"
cell_line_annotations <- read.delim(cell_line_annotations_fn,check.names=FALSE)

# rownames(cell_line_annotations) <- cell_line_annotations$DepMap_ID
# cl_translation <- cell_line_annotations[cl_names_depmap,"COSMICID"] # Contains NA so it is not complete

cell_line_annotations$depMapID <- as.character(cell_line_annotations$depMapID)
cell_line_annotations$Name <- as.character(cell_line_annotations$Name)
cell_line_annotations <- cell_line_annotations[,c("depMapID", "Name")]


cell_line_annotations <- cell_line_annotations[!is.na(cell_line_annotations$depMapID),]
rownames(cell_line_annotations) <- cell_line_annotations$depMapID


cl_names_depmap <- colnames(essentiality)
cl_translation <- cell_line_annotations[cl_names_depmap,"Name"]
sum(is.na(cl_translation)) # 71


colnames(essentiality) <- cl_translation
cl_essentiality <- colnames(essentiality)


genomic_features[cl_essentiality,"TISSUE_FACTOR"]
# -----------------------------------------------------------------------------------------------------------------------------------------------



# devtools::install_github("jimvine/rcellosaurus")
# library(rcellosaurus)
# cellosaurus <- read_cellosaurus_xml("C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/cellosaurus.xml")
# cell_lines <- cell_lines_all(cellosaurus)







cell_line_details

# 905933 ACH-000001
CVCL_E548 <- cell_lines_filter(cell_lines,
                               filter_by = "accession-secondary",
                               filter_term = "905933",
                               filter_type = "contains")


# BiocManager::install("depmap")
library(depmap)

depmap_cl_annotations <- as.data.frame(metadata_19Q3())
rownames(depmap_cl_annotations) <- depmap_cl_annotations$depmap_id
cl_translation <- depmap_cl_annotations[cl_names_depmap,"cosmic_id"] # Contains NA so it is not complete


sanger_dose_response_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/depmap/sanger-dose-response.csv"
sanger_dose_response <- read.csv(sanger_dose_response_fn, check.names=FALSE)

sanger_dose_response$ARXSPAN_ID <- as.character(sanger_dose_response$ARXSPAN_ID)
sanger_dose_response$COSMIC_ID <- as.character(sanger_dose_response$COSMIC_ID)


sanger_dose_response <- sanger_dose_response[,c("ARXSPAN_ID","COSMIC_ID")]
sanger_dose_response <- unique(sanger_dose_response)
emptyARXSPAN_ID <- which(sanger_dose_response$ARXSPAN_ID == "")
sanger_dose_response <- sanger_dose_response[-emptyARXSPAN_ID,]
duplicatedCOSMICID <- duplicated(sanger_dose_response$COSMIC_ID)
sanger_dose_response <- sanger_dose_response[!duplicatedCOSMICID,]

# rownames(sanger_dose_response) <- sanger_dose_response$ARXSPAN_ID
# cl_translation <- sanger_dose_response[cl_names_depmap,"COSMIC_ID"] # Contains NA so it is not complete

cl_names_cosmic <- as.character(genomic_features$COSMIC_ID)
rownames(sanger_dose_response) <- sanger_dose_response$COSMIC_ID

sanger_dose_response$ARXSPAN_ID <- as.character(sanger_dose_response$ARXSPAN_ID)

cl_translation <- sanger_dose_response[cl_names_cosmic,"ARXSPAN_ID"] # Contains NA so it is not complete




sanger_viability_fn <- "C:/Users/Carlos Castilla Ruiz/OneDrive - Tecnun/TECNUN/RevisiónDS/SANGER/depmap/sanger-viability.csv"
sanger_viability <- read.csv(sanger_viability_fn, check.names=FALSE)


sanger_viability$ARXSPAN_ID <- as.character(sanger_viability$ARXSPAN_ID)
sanger_viability$COSMIC_ID <- as.character(sanger_viability$COSMIC_ID)


sanger_viability <- sanger_viability[,c("ARXSPAN_ID","COSMIC_ID")]
sanger_viability <- unique(sanger_viability)

duplicatedCOSMICID <- duplicated(sanger_viability$COSMIC_ID)
sanger_viability <- sanger_viability[!duplicatedCOSMICID,]


# duplicatedARXSPAN_ID <- duplicated(sanger_viability$ARXSPAN_ID)
# sanger_viability <- sanger_viability[!duplicatedARXSPAN_ID,]
# sanger_viability <- sanger_viability[!is.na(sanger_viability$ARXSPAN_ID),]
# rownames(sanger_viability) <- sanger_viability$ARXSPAN_ID
# cl_translation <- sanger_viability[cl_names_depmap,"COSMIC_ID"] # Contains NA so it is not complete
# 


cl_names_cosmic <- as.character(genomic_features$COSMIC_ID)
rownames(sanger_viability) <- sanger_viability$COSMIC_ID

sanger_viability$ARXSPAN_ID <- as.character(sanger_viability$ARXSPAN_ID)

cl_translation <- sanger_viability[cl_names_cosmic,"ARXSPAN_ID"] # Contains NA so it is not complete




