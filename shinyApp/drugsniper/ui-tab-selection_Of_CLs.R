## ==================================================================================== ##
# MutationAchilles Shiny App for predicting transcript biomarkers.
# Copyright (C) 2019 Fernando Carazo & Carlos Castilla
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# You may contact the authors of this code, Fernando Carazo, at <fcarazo@tecnun.es>, and Carlos Castilla, at <ccastilla.1@tecnun.es>
## ==================================================================================== ##

# Tab #0
tabPanel("1. Select Cell Lines",
         
         sidebarLayout(
         # Sidebar
         sidebarPanel(           
           width = 3, # 3 Ferca value"
           flowLayout(
           div(id = "dD",   
            h4("Dataset"),
            radioButtons(inputId = "I_dataset", label = "Sensitivity dataset",choices = c("DEMETER","DEMETER2"), selected = "DEMETER")
           ),
           h4("Your Analysis"),
           h5("Primary sites selected:"),
           h6(textOutput("O_txtout")),
           h5("Number of samples:"),
           h6(textOutput("O_nCL_sel")),
           #br(),
           
           div(id = "cl_next_div", 
             actionButton("I_cl_next", "Next!", width = '100%',heigth = '200%', 
                          style="color: #ffffff; background-color: #77933C; border-color: #216b2c"), # Look for text redistribution inside the actionButton !!!!!!!!!!!!!
             tags$head(tags$style(type = "text/css",".btn-default { background-image: none;}")),
             tags$head(tags$style(type = "text/css",".btn-default:hover { background-image: none;}")),
           ),
           
           # hr(style="border-color: black; background-color: black;color: black;"),
           
           checkboxInput(inputId = "I_select_all_primarySite", label="Select all primary sites", value = FALSE, width = '100%'),
           checkboxGroupInput(inputId = "I_primarySite",
                              label = "Primary Site",
                              choiceValues = sort(unique(CCLE_sample_info_match$Primary.Site)),
                              choiceNames = sort(unique(CCLE_sample_info_match$Primary.Site_hr)),
                              selected = "lung"),
           # br(),
           div(id = "cb1",
            hr(style="border-color: black; background-color: black;color: black;"),
            checkboxInput(inputId = "I_select_all_subtypes", label="Select all subtypes", value = FALSE, width = '100%')
           ),
           # checkboxGroupInput(inputId ="I_subtype",  label = "Subtype"),
           shinyTree("I_subtype", checkbox = TRUE, themeIcons = FALSE, themeDots = TRUE, theme = 'proton', wholerow = FALSE), # default, default-dark, proton
           tags$head(tags$style(type = "text/css",".jstree-proton .jstree-clicked { background: #0000; color: #000; border-radius: 3px; box-shadow: inset 0 0 1px #0000; }")),
           tags$head(tags$style(type = "text/css",".jstree-proton .jstree-anchor {margin: 1px 0 10px; height: auto !important;}")),
           tags$head(tags$style(type = "text/css",".jstree-leaf {height: auto;}")),
           tags$head(tags$style(type = "text/css",".jstree-leaf a {height: auto !important;}")),
           tags$head(tags$style(type = "text/css",".jstree-proton a {white-space:normal !important; height: auto; }")),
           tags$head(tags$style(type = "text/css",".jstree-proton li > ins {vertical-align:top; }")),
           
           
           uiOutput("O_renderUI_subt_w")        
          )    
         ),
         
         # Main panel where all the info/results are shown
         mainPanel(       
           #tags$head(tags$style(type = "text/css", ".col-sm-8 {max-width: auto;}")),
           #tags$head(tags$style(type = "text/css", ".tabbable {max-width: 100%;}")),
           tabsetPanel(
             tabPanel(title = "Selected CLs",
                      h2("All Cell lines"),
                      withSpinner(plotOutput("O_gg_CL",height = 350, width = "100%")), # 1000)),
                      h2("Selected Cell lines"),
                      withSpinner(plotOutput("O_gg_CL_sbt", height = 350, width = "100%" )) # 1000))
                      ),
             tabPanel(title = "List of Selected CLs",
                      br(),
                      uiOutput("O_renderUI_Exclude_CLs"),
                      br(),
                      # br(),
                      DT::dataTableOutput("O_selected_CLs"),                      
                      downloadButton("O_selected_CLs_download","Download the data")
                      )
             )))

)
