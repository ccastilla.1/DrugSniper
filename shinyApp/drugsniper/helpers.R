
# CRAN's packages
pkgsNeeded <- c("shiny","shinyjs","shinythemes","shinyWidgets","ggplot2","scales","dplyr","Matrix","matrixStats","tidyr","readr","tictoc",
                "psych","pheatmap","RColorBrewer","progress","grid","shinycssloaders","rmarkdown","DT","ROCR","plotROC","stringr","pryr",
                "BiocManager","rlang","backports","RSQLite","shinydashboardPlus","shinyalert","readxl","shinyTree","shinyhelper")

pkgsInstalled <-  pkgsNeeded %in% rownames(installed.packages())
if (length(pkgsNeeded[!pkgsInstalled]) > 0){
  install.packages(pkgsNeeded[!pkgsInstalled], dependencies = T, repos = "https://cloud.r-project.org")
}


# Bioconductor's packages
pkgsNeeded <- c("impute", "limma", "STRINGdb", "qvalue")
pkgsInstalled <-  pkgsNeeded %in% rownames(installed.packages())
if (length(pkgsNeeded[!pkgsInstalled]) > 0){
  BiocManager::install(pkgsNeeded[!pkgsInstalled])
}



library(shiny)
library(shinyjs)
library(shinythemes)
library(shinyWidgets)
library(shinydashboardPlus)
library(shinyalert)

library(ggplot2)
library(scales)
library(dplyr)
library(Matrix)
library(matrixStats)
library(tidyr)
library(readr)
library(psych)
library(pheatmap)
library(RColorBrewer)
library(progress)
library(grid)
library(impute)
library(shinycssloaders)
library(rmarkdown)
library(DT)
library(limma)
library(STRINGdb)
library(qvalue)
library(ROCR)
library(plotROC)
library(tictoc)
library(readxl)
library(shinyTree)
library(shinyhelper)

# ------------------------------------------------------------
source("fun-auxFunctions_ModifySource.R")
source("fun-boxplotEssentiality_ModifySource.R")
source("fun-boxplotMutation_ModifySource.R")
source("fun-F_EG_Selection_ModifySource.R")
source("fun-F_filter_tr_gn_and_getGN_ModifySource.R")
source("fun-F_get_exp_genes_ModifySource.R")
source("fun-F_predictBiomarkers_trans_ModifySource.R")
source("fun-getPairs_tr2_ModifySource.R")
source("fun-ggmatplot_ModifySource.R")
source("fun-multiplot_ModifySource.R")

source("getPairs_mut.R")
source("predictBiomarkers_mut.R")


source("z_norm_table_by_cols.R")


