setwd("C:/Users/lucia/Desktop/project/Achilles-CCLE/R/general")
library(matrixStats)
library(dplyr)
#########################################
#parameters
#candidates
#demeter score < -2
#p_candidates2
#cancer type
#number of transcripts chosen trn 5000
#EG expression min log2(TPM+1)>1

prmtrs<- rep(0,20)
names(prmtrs)<-c("Ess_co","qntl","ntr","pval","nTOP_EG","nTOP_trns","ES_co","min_exp",9:20)
prmtrs["Ess_co"]<- -2
prmtrs["qntl"]<- 3
prmtrs["ntr"]<- 5000
prmtrs["pval"]<- 0.05
prmtrs["ES_co"]<- 3
prmtrs["min_exp"]<-1

#############################################################################################
#########    TO BE RUN ONLY IF THE PARAMETERS "Ess_co","qntl" OR "ntr" CHANGE ################
#########                     OTHERWISE GO TO NEXT SECTION                    ###########################
##############################################################################################

#load Demeter,getBM and CCLE data
AchilO<- read.table("ExpandedGeneZSolsCleaned.csv", header = TRUE, sep = ",")
load("getBM.RData")
CCLE<-read.table(file = 'trimmed_tpm.tsv', sep = '\t', header = TRUE)
load("CCLE_sample_info_all.RData")

#match CCLE and Demeter cell lines by name
CCLE<-CCLE[,2:ncol(CCLE)]
colnames(CCLE)<-rownames(CCLE_sample_info)
colnms_AchilO<-sub("_.*", "", colnames(AchilO))
matched_CL<- intersect(colnms_AchilO,colnames(CCLE))
matched_CL_index<-which(colnms_AchilO %in% matched_CL)


##################  1) TRANSCRIPTS   ######################################
##################  Data Preparation ######################################
# select matched CL
# remove transcripts with O expression in all CL, remove transcripts with no data
# select only transcript which for at least one cell line the expression level belongs 
# to the parmtrs["qntl"] quantile of the expression levels for this particular cell line
# select prmtrs["ntr"]transcripts with highest variance

transcripts<-as.matrix(CCLE[,matched_CL])
rownames(transcripts)<-getBM[,1]


quant<-rep(0,ncol=ncol(transcripts))
n=ncol(transcripts)
for (i in 1:ncol(transcripts)){
  quant[i]<-quantile(transcripts[,i])[prmtrs["qntl"]]
}
quantmat<-matrix(rep(quant,nrow(transcripts)), ncol = length(matched_CL),byrow = TRUE)
decision<-transcripts>quantmat
a<-rep(0,nrow(transcripts))
a<-rowSums(decision)
a2<-which(a>0)
transcripts<-transcripts[a2,]


vars <- rowMads(transcripts)
ord <- order(vars,decreasing = T)
iTr_ord <- ord[1:prmtrs["ntr"]]
transcripts<-transcripts[iTr_ord,]



#########################  2) Essentiality   ############################
#########################  Data Preparation  ###########################
# select matched CL
# populate missing values
# make Essentiality dicotomous
# remove genes not essential for any CL

rownames(AchilO)<-AchilO[,1]
AchilO<-as.matrix(AchilO[,matched_CL_index])
#rownames(AchilO)<-AchilO[,1]
#AchilO<-AchilO[,-1]
Salida <- impute.knn(as.matrix(AchilO) ,k = 10, rowmax = 0.75, colmax = 0.8, maxp = 1500, rng.seed=362436069)
AchilO <- Salida$data
Essentiality_d<-matrix(prmtrs["Ess_co"],ncol=ncol(AchilO),nrow=nrow(AchilO))
rownames(Essentiality_d)<-rownames(AchilO)
Essentiality_d<-(AchilO<Essentiality_d)*1
a<- rowSums(Essentiality_d)
Essentiality_d<-Essentiality_d[which(a>0),]
Essentiality<-AchilO[which(a>0),]

#################################################################################
# save transcripts and Essentiality_d
save(transcripts,Essentiality,Essentiality_d, getBM, file = "data.RData")
##############################################################################


############################################################################################
#######################   ESSENTIAL GENES  ##################################################
###############################################################################################

# the original list has candidates genes.Of these, 25 genes are in Achilles.
#candidates<- as.matrix(read.csv("MOLECULAS EN CIMA MOD.csv"))
#p_candidates<-matrix(0,ncol=2,nrow=length(intersect(rownames(AchilO),candidates)))
#p_candidates[,1]<-intersect(rownames(AchilO),candidates)
#p_non_candidates<-as.matrix(setdiff(candidates,p_candidates))
###### Prospection
### input: p_candidates
#
#for(i in 1:nrow(p_candidates)){
# target<-p_candidates[i,1]
# main_title<-paste(target)
# info_target<-(cbind(AchilO[target,],(AchilO[target,]<(-2))*1,Mutations,sub(".*_", "", colnames(AchilO))))
# colnames(info_target)<-c("Essentiality Q","Essentiality D","Mutations","Type")
# table(info_target[which(info_target[,2]==1),4])
# table(info_target[which(info_target[,2]==0),4])
# AAA1<-(as.matrix(table(info_target[,4])))
# AAA2<-as.matrix(table(info_target[which(info_target[,2]==1),4]))
  
CL_CancerType<-as.matrix(table(sub(".*_", "", colnames(Essentiality_d))))

#create 3 tables

CL_frequencies<-rowSums(Essentiality_d)/ncol(Essentiality_d)*100

CL_expected<-matrix(CL_CancerType,nrow=8239,ncol=length(CL_CancerType),byrow=TRUE)
colnames(CL_expected)<-rownames(CL_CancerType)
rownames(CL_expected)<-rownames(Essentiality_d)
CL_expected<-floor(0.5+CL_frequencies*CL_expected/100)

CL_observed<- matrix(0,ncol=length(CL_CancerType),nrow=nrow(Essentiality_d))
colnames(CL_observed)<-rownames(CL_CancerType)
rownames(CL_observed)<-rownames(Essentiality_d)
for(i in 1:length(CL_CancerType)){
  b<-rownames(CL_CancerType)[i]
  cc<-which(sub(".*_", "", colnames(Essentiality_d))==b)
  CL_observed[,i]<-rowSums(Essentiality_d [,cc])}

Enrichment<-CL_observed/CL_expected
Enrichment[is.nan(Enrichment)] = 0

##################################################
Cancer_type<-"BREAST"
Cancer_subtype<-""


Cancer_Type_CL<-CCLE_sample_info$CCLE.name[which(CCLE_sample_info$Primary.Site %in% tolower(Cancer_type))]
table(CCLE_sample_info[CCLE_sample_info[,4]==tolower(Cancer_type),5])
Cancer_Type_CL<-intersect(Cancer_Type_CL,colnames(Essentiality_d))
# 34 CL


################## Identification of Essential Genes ##########################################
########## a gene is considered as essential if its enrichment ratio is over 4 ################

aa<-which(Enrichment[,Cancer_type]> prmtrs["ES_co"] &!Enrichment[,Cancer_type]== Inf)
EG<-names(aa)
bb<-which(Enrichment[,Cancer_type]== Inf)


Essential_Genes<-matrix(0,ncol=3,nrow = length(aa))
colnames(Essential_Genes)<-c("Enrichment","Ess CL","Median for ES CL")
rownames(Essential_Genes)<-names(aa)
Essential_Genes[,1]<-Enrichment[names(aa),Cancer_type]
Essential_Genes[,2]<-CL_observed[aa,Cancer_type]
AAA<-Essentiality[names(aa),Cancer_Type_CL]*Essentiality_d[names(aa),Cancer_Type_CL]
aaa<-rep(0,length(aa))
for(i in 1:length(aa)){
   aaa[i]<-median(AAA[i,][AAA[i,]<0])}
Essential_Genes[,3]<-t(aaa)
Essential_Genes<-Essential_Genes[order(-Essential_Genes[,1],-Essential_Genes[,2],Essential_Genes[,3]),]

Essential_Genes_Inf<-matrix(0,ncol=3,nrow = length(bb))
colnames(Essential_Genes_Inf)<-c("Enrichment","Ess CL","Median for ES CL")
rownames(Essential_Genes_Inf)<-names(bb)
Essential_Genes_Inf[,1]<-Enrichment[names(bb),Cancer_type]
Essential_Genes_Inf[,2]<-CL_observed[bb,Cancer_type]
BBB<-Essentiality[names(bb),Cancer_Type_CL]*Essentiality_d[names(bb),Cancer_Type_CL]
bbb<-rep(0,length(bb))
for(i in 1:length(bb)){
  bbb[i]<-median(BBB[i,][BBB[i,]<0])}
Essential_Genes_Inf[,3]<-t(bbb)
Essential_Genes<-Essential_Genes[order(-Essential_Genes[,2],-Essential_Genes[,3]),]


###### Essential Genes EXPRESSION CHECK ###################################################

# collapsing transcripts information into gene expression
transcripts_c<-as.matrix(CCLE[,matched_CL])
rownames(transcripts_c)<-getBM[,1]
transcripts_c<-na.omit(transcripts_c)
rownames(getBM)<-getBM[,1]
Transcripts_to_genes<-getBM[rownames(transcripts_c),4]
genes_expre<-data.frame(transcripts_c,Transcripts_to_genes)
num_cols<-ncol(genes_expre)
colnames(genes_expre)[num_cols]<-"Gene"

genes_expression<-summaryBy(.~Gene, data=genes_expre, FUN=sum)
rownames(genes_expression)<-genes_expression[,1]
genes_expression<-genes_expression[,2:num_cols]
colnames(genes_expression)<-substr(colnames(genes_expression),1,nchar(colnames(genes_expression))-4)


# Check if the essential genes are expressed
Cancer_Type_CL_short<-CCLE_sample_info[CCLE_sample_info[,2]%in% Cancer_Type_CL,1]
EG_genes_expression<-genes_expression[names(aa),Cancer_Type_CL_short]
EG_genes_expression<-log2(EG_genes_expression+1)
check<-matrix(prmtrs["min_exp"],ncol=length(Cancer_Type_CL),nrow=length(aa))
EG_genes_expression_checked<-(EG_genes_expression>check)*1
comparison_matrix<-EG_genes_expression_checked*Essentiality_d[names(aa),Cancer_Type_CL]

result<-cbind(rowSums(Essentiality_d[names(aa),Cancer_Type_CL]),rowSums(comparison_matrix),
              (rowSums(Essentiality_d[names(aa),Cancer_Type_CL])-rowSums(comparison_matrix)),rowSums(Essentiality_d[names(aa),Cancer_Type_CL])==rowSums(comparison_matrix))

#####################################################################################################



#create 3 tables

CL_expected_2<-CL_expected
CL_expected_2[which(CL_expected_2 ==0)]<-1
Enrichment_2<-CL_observed/CL_expected_2
Enrichment_2[is.nan(Enrichment)] = 0

max(Enrichment[1,])





















######## MUTATIONS ANALYSIS ###########################################




##### TRANSCRIPTS ANALYSIS #############################################
###### genes names check CCLE vs Achiles
tobechanged<-which(rownames(Essentiality_d)=="WARS2-IT1")
rownames(Essentiality_d)[6784]<-"WARS2_IT1"
EG[102]<- "WARS2_IT1"
which(rownames(Essential_Genes)=="WARS2-IT1" )#72
which(rownames(Essential_Genes)=="ATP5J2-PTCD1")#123
rownames(Essential_Genes)[72]<-"WARS2_IT1"
rownames(Essential_Genes)[123]<-"ATP5J2_PTCD1"




n=length(aa)

lhs_d  <- paste("model_d",    rownames(Essential_Genes)[1:n],     sep="_")
rhs_d  <- paste("t(rbind(t(Essentiality_d[",1:n,",]),transcripts))", sep="")
eq_d   <- paste(paste(lhs_d, rhs_d, sep="<-"), collapse=";")
eval(parse(text=eq_d))

Dmatrix


kkkk_d<-matrix(0,ncol=nrow(new_transcripts),nrow=nrow(genes))
colnames(kkkk_d)<-rownames(new_transcripts)
rownames(kkkk_d)<-EG_315[,1]






















sort(Enrichment[3,],partial=n-1)[n-1]


  par(mfrow = c(1, 1),mar=c(2,2,2,2))
  plot(sort((as.numeric(info_target[,1])),decreasing=T),xlab="Number of Cell Lines",ylab="Demeter Score",main=main_title)
  abline(h=-2,col="red")
  
  if(num_essential_CL>0){
    p_candidates[i,2]<-1
    par(mfrow = c(2, 2),mar=c(2,2,2,2))
    barplot(frequencies[,2],las=2,col=c("darkblue"),ylim = c(0,40),cex.names=0.5)
    par(new=T)
    barplot(frequencies[,3],main="Target Essential",ylab="N Cell Lines",cex.main=1,las=2,ylim=c(0,40),border="orange",density=0,cex.names =0.5)
    barplot(sort(table(info_target[which(info_target[,2]==0),4]),decreasing = T),main="Target Non Essential",
            ylab="N Cell Lines",las=2,cex.main=1,cex.names=0.5,col=c("darkgreen"))
    boxplot(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Essentiality D"]),
            main="Demeter vs Essentiality",ylab="Demeter score",xlab="Essentiality D",cex.main=1)
    p<-c(0,0)
    p[1]<-wilcox.test(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Essentiality D"]))$p.value
    if(p[1]<0.05){
      text(5,"*",cex=2)
    }
    
    boxplot(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Mutations"]),
            main="Demeter vs Mutation",ylab="Demeter score",xlab="Mutation",cex.main=1)
    p[2]<-wilcox.test(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Mutations"]))$p.value
    if(p[2]<0.05){
      text(5,"*",cex=2)
    }
    
    title(main=main_title,outer=T)
  }
}





