
# the original list has candidates genes.Of these, 25 genes are in Achilles.
candidates<- as.matrix(read.csv("./data/input/MOLECULAS EN CIMA MOD.csv"))



F_Inhibitors<- function(Essentiality,Mutations,candidates,co_pess_iCL,co_enr){
  prmtrs<-rep(0,2)
  names(prmtrs)<-c("co_pess_iCL","co_enr")
  prmtrs["co_pess_iCL"]<-co_pess_iCL   #0.2  min % of CL for which a gene has to be essential
  prmtrs["co_enr"]<-co_enr     #1.5  min enrichment ratio
library(dplyr)
library(stats)
library(impute)
output <- impute.knn(as.matrix(Essentiality) ,k = 10, rowmax = 0.75, colmax = 0.8, maxp = 1500, rng.seed=362436069)
Essentiality <-output$data
rm(output)
p_candidates<-matrix(0,ncol=2,nrow=length(intersect(rownames(Essentiality),candidates)))
colnames(p_candidates)<-c("target","essential","targetable Diseases",c(sort(unique(CCLE_demeter_sample_info$Primary.Disease))))
p_candidates[,1]<-intersect(rownames(Essentiality),candidates)
p_non_candidates<-as.matrix(setdiff(candidates,p_candidates))
Aux1<-matrix(0,nrow=length(unique(CCLE_demeter_sample_info$Primary.Disease)),ncol = nrow(p_candidates))
Aux2<-matrix(0,nrow=length(unique(CCLE_demeter_sample_info$Primary.Disease)),ncol = nrow(p_candidates))
M1<-matrix(prmtrs["co_enr"],ncol=ncol(Aux1),nrow=nrow(Aux1))
M2<-matrix(prmtrs["co_pess_iCL"],ncol=ncol(Aux2),nrow=nrow(Aux2))

###### Prospection
a<-match(p_candidates[,1],colnames(Mutations))
a<-a[!is.na(a)]
Mutations_can<-Mutations[,a]
Mutations_target<- rep(5,ncol(Essentiality))
### input: p_candidates

for(i in 1:nrow(p_candidates)){
  target<-p_candidates[i,1]
  main_title<-paste(target)
  file_name<-paste("./data/output/",main_title,".jpg")
  file_name2<-paste("./data/output/",main_title,"mutations.jpg")
  b<-p_candidates[i,1]%in%colnames(Mutations_can)
  if(b=="TRUE"){
    Mutations_target<-Mutations_can[,target]
  }
  info_target<-t(rbind(Essentiality[target,],(Essentiality[target,]<(-2))*1,Mutations_target,CCLE_demeter_sample_info$Primary.Disease))
  colnames(info_target)<-c("Essentiality Q","Essentiality D","Mutations","Type")
  table(info_target[which(info_target[,2]==1),4])
  table(info_target[which(info_target[,2]==0),4])
  AAA1<-(as.matrix(table(info_target[,4])))
  AAA2<-as.matrix(table(info_target[which(info_target[,2]==1),4]))
  
  frequencies<-matrix(0,ncol=6,nrow(AAA1))
  rownames(frequencies)<-rownames(AAA1)
  colnames(frequencies)<-c("N_CL","EXPECTED Essential","OBSERVED Essential","Enrichment Ratio","%Essential CL","over-expression")
  frequencies[,1]<-AAA1[,1]
  num_essential_CL<-sum(as.numeric(info_target[,2]))
  num_non_essential_CL<-ncol(Essentiality)-num_essential_CL
  frequencies[,2]<-round(frequencies[,1]/sum(frequencies[,1])*num_essential_CL,2)
  frequencies[which(rownames(frequencies)%in%rownames(AAA2)),3]<-AAA2[which(rownames(AAA2)%in%rownames(frequencies)),1]
  frequencies[,4]<-round(frequencies[,3]/frequencies[,2],2)
  frequencies[frequencies[,2]==0 & frequencies[,3]==0,4]<-0
  frequencies[,5]<-frequencies[,3]/frequencies[,1]
  Aux1[,i]<-frequencies[,4]
  Aux2[,i]<-frequencies[,5]

  
 
  if(num_essential_CL==0){
    jpeg(file_name)
  par(mfrow = c(1, 1),mar=c(2,2,2,2))
  plot(sort((as.numeric(info_target[,1])),decreasing=T),xlab="Number of Cell Lines",ylab="Demeter Score",main="Essentiality per cell line")
  abline(h=-2,col="red")
  dev.off()}
  
  if(num_essential_CL>0){
    if(target%in%colnames(Mutations_can)==TRUE){
    jpeg(file_name) 
    p_candidates[i,2]<-1
    par(mfrow = c(2, 2),mar=c(3,2,2.5,2))
    lim<-max(frequencies[,2],frequencies[,3])
    lim2<-max(frequencies[,4])*1.2
   
    plot(sort((as.numeric(info_target[,1])),decreasing=T),xlab="Number of Cell Lines",ylab="Demeter Score",main="Essentiality per cell line",cex.main=1)
    abline(h=-2,col="red")
    boxplot(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Essentiality D"]),
            main="Demeter vs Essentiality",ylab="Demeter score",xlab="Essentiality D",cex.main=1)
    p<-c(0,0)
    p[1]<-wilcox.test(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Essentiality D"]))$p.value
    if(p[1]<0.05){
      text(5,"*",cex=2)}
  
    
    barplot(sort(frequencies[,4],decreasing = TRUE),col=c("darkgreen"),ylim = c(0,lim2),main="Enrichment Ratio",
            ylab="Enr_Ratio",cex.main=1,las=2)
    barplot(frequencies[,2],las=2,col=c("darkblue"),ylim = c(0,lim),cex.names=0.5)
    par(new=T)
    barplot(frequencies[,3],main="Obs vs Exp Essential CL ",ylab="N Cell Lines",cex.main=1,las=2,ylim=c(0,lim),border="orange",density=0,cex.names =0.5)
    title(main=main_title,outer=T)
    dev.off()
    jpeg(file_name2) 
    boxplot(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Mutations"]),
    main="Demeter vs Mutation",ylab="Demeter score",xlab="Mutation",cex.main=1)
    p[2]<-wilcox.test(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Mutations"]))$p.value
    if(p[2]<0.05){
      text(5,"*",cex=2)
    }
    dev.off()
    }
    }

  
  if(num_essential_CL>0){
    if(target%in%colnames(Mutations_can)==FALSE){
      p_candidates[i,2]<-1
      jpeg(file_name) 
      par(mfrow = c(2, 2),mar=c(3,2,2,2))
      lim<-max(frequencies[,2],frequencies[,3])
      lim2<-max(frequencies[,4])*1.2
      
      plot(sort((as.numeric(info_target[,1])),decreasing=T),xlab="Number of Cell Lines",ylab="Demeter Score",main="Essentiality per cell line",cex.main=1)
      abline(h=-2,col="red")
      
      boxplot(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Essentiality D"]),
              main="Demeter vs Essentiality",ylab="Demeter score",xlab="Essentiality D",cex.main=1)
      p<-c(0,0)
      p[1]<-wilcox.test(as.numeric(info_target[,"Essentiality Q"])~as.numeric(info_target[,"Essentiality D"]))$p.value
      if(p[1]<0.05){
        text(5,"*",cex=2)
      }
      
      barplot(sort(frequencies[,4],decreasing = TRUE),col=c("darkgreen"),ylim = c(0,lim2),main="Enrichment Ratio",
              ylab="Enr_Ratio",cex.main=1,las=2)
      barplot(frequencies[,2],las=2,col=c("darkblue"),ylim = c(0,lim),cex.names=0.5)
      par(new=T)
      barplot(frequencies[,3],main="Obs vs Exp Essential CL",ylab="N Cell Lines",cex.main=1,las=2,ylim=c(0,lim),border="orange",density=0,cex.names =0.5)
      title(main=main_title,outer=T)
      dev.off()
      
    }}
}  

Aux3<-(Aux1>M1)*1
Aux4<-(Aux2>M2)*1
Aux<-Aux3*Aux4
Aux_f<-Aux*Aux1
p_candidates<-cbind(p_candidates,colSums(Aux),t(Aux_f))
s<-c(nrow(p_candidates),sum(as.numeric(p_candidates[,2])),sum(Aux),rowSums(Aux))
p_candidates<-rbind(p_candidates,s)
colnames(p_candidates)<-c("target","essential","targetable Diseases",c(sort(unique(CCLE_demeter_sample_info$Primary.Disease))))
rownames(p_candidates)<-p_candidates[,1]
rm(Aux,Aux3,Aux4,M1,M2,s,lim,lim2,main_title,num_essential_CL,num_non_essential_CL,b,AAA1,AAA2,p,Aux_f,Aux1,Aux2,a)
good_candidates<-p_candidates[which(p_candidates[-nrow(p_candidates),3]>0),1]

return(good_candidates,p_candidates)
}
