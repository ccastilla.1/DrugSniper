source("../../code/Wilcoxon.z.matrix.R")
source("./code/boxplotMutation.R")
source("./code/getPairs_mut.R")
source("./code/predictBiomarkers_mut.R")

# Gene_E <- iGn
Essentiality <- CCLE_demeter_match
CLxMut <- as.matrix(CCLE_sample_info_match[,6:ncol(CCLE_sample_info_match)])
CLxMut[is.na(CLxMut)] <- 0

# rm(list = setdiff(ls(), c("Essentiality", "CLxMut", "iCL", "iGn)))

co_pMut_iCL = 0.1


iGn <- match(rownames(esGene), rownames(Essentiality))
predictBiomarkers <- predictBiomarkers_mut(Essentiality = Essentiality, CLxMut = CLxMut, co_pMut_iCL = 0.2, iGn = iGn, iCL = iCL, restCL = TRUE)

MagicCube_Mut <- predictBiomarkers$MagicCube_Mut
Fit_mut_list <- predictBiomarkers$Fit_mut_list

# View biomarkers for a gene
View(MagicCube_Mut["PLK1", ,])

# View gene essentials for a mutation
View(MagicCube_Mut[,"FGFR3" ,])

# View volcano plot for a mutation
mutation <- "EP300"
h <- 5
FitA <- Fit_mut_list[[mutation]]
volcanoplot(FitA, highlight = h, names = rownames(Essentiality)[iGn], main = paste("Essential genes of mutation ", mutation, "\n(left: treatment when mutated)"), xlab = "Increment of Essentiality")

Gene_E <- "PLK1"
boxplotMutation(Essentiality, CLxMut, iCL, Gene_E, Mutation = "EP300", tag = tag)

aux <- data.frame(MagicCube_Mut[,"TP53",])
boxplot(-log10(aux$P.Value) ~ aux$Same_Dir)



boxplotMutation(Essentiality, CLxMut, iCL, Gene_E = "AP2M1", Mutation = "CXCL12", tag = tag)

# Get Pairs
load("./data/output/geneInfo.RData")

Pairs <- getPairs_mut(predictBiomarkers, co_FDR = 0.7, String = FALSE, Dir = FALSE, co_DeltaEs = 3)

Pairs <- merge(Pairs, Gene_Info, by.x = "Gene_E", by.y = "hgnc_symbol", all.x = T, all.y = F)
Pairs <- Pairs[order(Pairs$P.Value), ]
rownames(Pairs) <- NULL

# write.csv2(Pairs, file = "./data/output/results/2017-12-11 AML_Mutations_magicCube/Pairs_AML2.csv")

# Boxplot mutations
j <- 1

fileDest <- "./data/output/results/2017-12-11 AML_Mutations_magicCube/"
for(j in 1:nrow(Pairs)){
  Gene_E <- Pairs$Gene_E[j]
  Mutation <- Pairs$Mutation[j]
  
  pdf(file = paste0(fileDest, sprintf("%s.%s-%s_boxplot.pdf", j, Gene_E, Mutation)), width = 12, height = 4.3)
  boxplotMutation(Essentiality, CLxMut, iCL, Gene_E, Mutation = Mutation, tag = tag)
  dev.off()
  }




