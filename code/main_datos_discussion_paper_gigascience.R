library(dplyr)
library(readr)
library(Matrix)
library(matrixStats)
library(psych)
library(tidyr)
library(ggplot2)
library(pheatmap)
library(RColorBrewer)
library(progress)
library(biomaRt)
library(grid)
library(qvalue)
library(impute)
library(WriteXLS)
source("../../code/ggmatplot.R")
source("./code/auxFunctions.R")
source("../../code/multiplot.R")
source("../../code/Wilcoxon.z.matrix.R")
source("../../code/progressTime.R")
source("../../code/GetFDR.R")
source("../../code/Wilcoxon.z.matrix.R")
source("./code/boxplotMutation.R")
source("./code/boxplotEssentiality.R")
source("./code/getPairs_mut.R")
source("./code/predictBiomarkers_mut.R")
source("./code_lu/F_EG_Selection.R")
source("./code_lu/F_predictBiomarkers_trans.R")
source("./code/getPairs_tr.R")
source("Z:/BACKUP_GRUPO/FernandoCarazo/ProyectosCompletos/hnRNP_Huelga_Analysis/GT_analysis/GT_analysis3.R")
source("./code/F_get_exp_genes.R")
source("./code/F_filter_tr_gn_and_getGN.R")
source("./code/getPairs_tr2.R")

load("./shinyApp/transcriptachilles/dataShiny/DataForShiny2.RData")


# Essential genes are expressed? ------------------------------------------


# Prepare data
irow <- match(rownames(CCLE_demeter_match), rownames(CCLE_tpm_gn_match))
CCLE_demeter_match_bin <- (CCLE_demeter_match[!is.na(irow), ] < -2) + 0
irow <- irow [!is.na(irow)]
exp_gEss <- CCLE_tpm_gn_match[irow, ]

# Expressed?

Meds <- data.frame(Gn = rownames(CCLE_demeter_match_bin), N_Ess = NA, Med_exp = NA)

for(i in 1:nrow(CCLE_demeter_match_bin)){
  gn <- rownames(CCLE_demeter_match_bin)[i]
  iEss <- which(CCLE_demeter_match_bin[i, ] == 1)
  aux_exp <- exp_gEss[i, iEss]
  Meds$N_Ess[i] <- sum(CCLE_demeter_match_bin[i, ] == 1)
  
  Meds$Med_exp[i] <- round(quantile(aux_exp, 0.25))
}

Meds <- Meds[Meds$N_Ess > 4, ]
sum(Meds$Med_exp > 1) / nrow(Meds) # 0.66

nEss <- sum(CCLE_demeter_match_bin == 1)
nEss_exp <- sum((CCLE_demeter_match_bin == 1) & (exp_gEss >= 1))
nEss_exp / nEss # 0.81

# Transcript expression
sum(rowQuantiles(CCLE_tpm_match, probs = 0.99) < 1) / nrow(CCLE_tpm_match) # 

#  ------------------------------------------------------------------------


CCLE_sample_info_match$ann <- data.frame(Type = paste(CCLE_sample_info_match$Primary.Site, CCLE_sample_info_match$Subtype, sep = "_"))
ann <- data.frame(Type = paste(CCLE_sample_info_match$Primary.Site, CCLE_sample_info_match$Subtype, sep = "_"))
rownames(ann) <- CCLE_sample_info_match$CCLE.name

# ann <- data.frame(Type = paste(CCLE_sample_info_match$Subtype))

nSel <- 1000
mads <- rowMads(CCLE_tpm_gn_match)
iExp <- order(mads, decreasing = T)[1:nSel]


CCLE_tpm_gn_match_med <- data.frame(t(CCLE_tpm_gn_match[iExp, ]))
CCLE_tpm_gn_match_med$Type <- ann$Type


aux <- CCLE_tpm_gn_match_med %>% group_by(Type) %>% summarize_all(funs(median))
rownames(aux) <- aux$Type
aux <- aux %>% dplyr::select(-Type)

pheatmap(cor(t(aux)), show_colnames = F, show_rownames = T, cutree_rows = 10)



# NSamples ----------------------------------------------------------------


path <- "./data/output/results/2018-07-12 Hematological tumors/"

namesT <- names(which(table(ann$Type) >= 7))

namesT <- namesT[-grep("leukemia", namesT)]

i <- 1

for(i in 5:length(namesT)){
  
  tag <- namesT[i]
  if(!(dir.exists(paste0(path, tag)))) dir.create(paste0(path, tag))
  
  iCL <- which(CCLE_sample_info_match$ann == namesT[i])
  
  esGene <- F_EG_Selection(Essentiality = CCLE_demeter_match, Exp_gn = CCLE_tpm_gn_match, iCL = iCL, co_enr=2, co_pess_iCL=0.25, co_exp=1,
                           qntl_exp=0.75, co_demeter=-2, impute = FALSE)
  
  write.csv2(esGene, paste0(path, tag, "/", "TranscriptAchilles_EssentialGenes.csv"), quote = F)
  
  
  # Filter Transcript expression
  Filt <- filter_tr_gn_and_getGN(Exp_trancripts = CCLE_tpm_match, getBM = getBM, iCL = iCL, qt = 0.5)
  iT_exp <- match(Filt$T_filt, rownames(CCLE_tpm_match))
  iG_exp <- match(Filt$G_filt, rownames(Filt$Exp_gn))
  Exp_genes <- Filt$Exp_gn
  
  # Get essential genes index
  iGn <- match(rownames(esGene), rownames(CCLE_demeter_match))
  
  # Predict Transcript Biomarkers
  predictBiomarkers_tr <- F_predictBiomarkers_trans(Essentiality = CCLE_demeter_match, 
                                                    Exp_trancripts = CCLE_tpm_match, 
                                                    Exp_genes = Exp_genes,
                                                    iGn_ess = iGn,
                                                    iT_exp = iT_exp, 
                                                    iG_exp = iG_exp, 
                                                    iCL = iCL, 
                                                    getBM = getBM, 
                                                    co_demeter = -2, 
                                                    impute = F, restCL = F, 
                                                    string = F, shiny = F)
  
  Pairs_tr <- getPairs_tr_list(Tr_Bmrk_list = predictBiomarkers_tr$Tr_Bmrk_list, getBM = getBM, co_PV = 0.05, co_log2FC = 1.5)
  
  
  iSel <- which((Pairs_tr$P.Value < 0.001) &
                  (abs(Pairs_tr$logFC) > 1.5) &
                  (Pairs_tr$lfdr < 0.6))
  
  write.csv2(Pairs_tr[iSel, ], paste0(path, tag, "/", "TranscriptAchilles_GenomeWideBmrkrs.csv"), quote = F, row.names = F)
  
}
