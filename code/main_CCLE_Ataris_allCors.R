library(dplyr)
library(readr)
library(Matrix)
library(matrixStats)
library(psych)
library(tidyr)
library(ggplot2)
library(pheatmap)
library(RColorBrewer)
library(progress)
library(biomaRt)
library(grid)
library(impute)
library(WriteXLS)
source("../../code/ggmatplot.R")
source("./code/auxFunctions.R")
source("../../code/multiplot.R")


##################################################
# 1) Load data
##################################################

load("./data/output/preparingData4.RData")
CCLE_tpm_ALL <- as.data.frame(CCLE_tpm_ALL)
rownames(CCLE_tpm_ALL) <- getBM$Transcript_ID

## run for ATARiS-----------------------------------------------------------------
experiment = "ATARiS"
iA <- match(colnames(CCLE_tpm_ALL), colnames(CCLE_ataris))
iT_A <- which(!is.na(iA))
iA <- iA[!is.na(iA)]
CCLE_ataris <- CCLE_ataris[,c(1,iA)]
rm(iA)

CCLE_tpm <- CCLE_tpm_ALL[,c(1,iT_A)]
CCLE_sample_info <- CCLE_sample_info_ALL[(iT_A-1),]
rm(iT_A)
# Make syntactically valid names out of character vectors and set as rownames.
rownames(CCLE_ataris)<- make.names(CCLE_ataris[,1],unique = T)
aux_nm <- CCLE_ataris$DESCRIPTION
CCLE_ataris <- as.matrix(CCLE_ataris[,-1])
rownames(CCLE_ataris) <- aux_nm
dim(CCLE_ataris) #5711  172
CCLE_ataris <- impute.knn(CCLE_ataris)$data

identical(colnames(CCLE_ataris), colnames(CCLE_tpm)[-1])

## run for Bayes Factors-----------------------------------------------------------------
# experiment = "BayesFactor"
# iB <- match(colnames(CCLE_tpm_ALL), colnames(CCLE_bayesFactor))
# iT_B <- which(!is.na(iB))
# iB <- iB[!is.na(iB)]
# CCLE_bayesFactor <- CCLE_bayesFactor[,iB]
# dim(CCLE_bayesFactor) #11527   176
# rm(iB)
# 
# CCLE_tpm <- CCLE_tpm_ALL[,c(1,iT_B)]
# CCLE_sample_info <- CCLE_sample_info_ALL[(iT_B-1),]
# rm(iT_B)
# # Make syntactically valid names out of character vectors and set as rownames.
# 
# CCLE_bayesFactor <- as.matrix(CCLE_bayesFactor)
# rownames(CCLE_bayesFactor) <- as.character(rownames(CCLE_bayesFactor))
# CCLE_bayesFactor <- impute.knn(CCLE_bayesFactor)$data
# 
# identical(colnames(CCLE_bayesFactor), colnames(CCLE_tpm)[-1])
# identical(colnames(CCLE_bayesFactor),  CCLE_sample_info$CCLE.name)


##################################################
# 2) Prepare Data
##################################################

CCLE_tpm_log2 <- log2(1 + as.matrix(CCLE_tpm[,-1]))
rownames(CCLE_tpm_log2) <- getBM$Transcript_ID

# building gene expression
CCLE_tpm_gn <- cbind(gene_ID = getBM[,c(2)], CCLE_tpm[,-1])
CCLE_tpm_gn <- as.data.frame(CCLE_tpm_gn %>% group_by(Gene_ID) %>% summarise_each(funs(sum)))
rownames(CCLE_tpm_gn) <- unlist(CCLE_tpm_gn[,1])
CCLE_tpm_gn <- CCLE_tpm_gn %>% dplyr::select(-Gene_ID)
CCLE_tpm_gn <- as.data.frame(CCLE_tpm_gn)
iG <- match(rownames(CCLE_tpm_gn), getBM$Gene_ID)
getBM_gn <- getBM[iG,]
rownames(CCLE_tpm_gn) <- make.unique(getBM_gn$Gene_name)

CCLE_tpm_gn_log2 <- log2(1 + CCLE_tpm_gn)
##################################################
# Global analysis
##################################################

# # Primary Site
# tb <- within(CCLE_sample_info,
#              Primary.Site <- factor(Primary.Site,
#                                levels=names(sort(table(Primary.Site),
#                                                  decreasing=TRUE))))
# ggplot(tb, aes(Primary.Site)) + geom_bar() +  coord_flip()  + ggtitle("Number of CL matched. CCLE & ATARis")
# rm(tb)
# 
# # PrimarySite with Subtype
# iCL <- which(CCLE_sample_info$Subtype %in% c("adenocarcinoma", "squamous_cell_carcinoma"))
# tb <- within(CCLE_sample_info[iCL, ],
#              Subtype <- factor(Subtype,
#                                levels=names(sort(table(Subtype),
#                                                  decreasing=TRUE))))
# ggplot(tb, aes(Subtype)) + geom_bar(aes(fill=Primary.Site))  + ggtitle("Number of CL matched. CCLE & ATARis")
# rm(tb)
# 
# 
# ### Correlation between all samples
# iCL <- 1:ncol(CCLE_tpm_log2)
# method <- "spearman"
# 
# # wrong samples
# iCL <- setdiff(iCL, c(43, 136, 145, 143, 147))
# 
# annotation_col = data.frame(Primary.Site = CCLE_sample_info$Primary.Site[iCL], Subtype = CCLE_sample_info$Subtype[iCL])
# rownames(annotation_col) <- colnames(CCLE_tpm_log2)[iCL]
# 
# pheatmap(cor(CCLE_tpm_log2[, iCL], method = method),
#          labels_row = iCL,
#          annotation_col = annotation_col,
#          labels_col = CCLE_sample_info$Primary.Site[iCL],
#          cluster_rows = T,
#          legend_labels = c("0", "1"),
#          color = colorRampPalette((brewer.pal(n = 7, name ="YlOrRd")))(100),
#          cutree_cols = 4,
#          width = 20,
#          height = 20,
#          filename = "CCLE-cluster.pdf",
#          treeheight_row = 0,
#          main = "Correlation of Samples (transcripts)")
# 
# iCL <- which(CCLE_sample_info$Disease %in% c("LCLL", "MM"))
# iCL <- which(CCLE_sample_info$Disease %in% c("COAD", "OV", "ESCA"))
# iCL <- 1:ncol(CCLE_tpm_log2)
# 
# tb <- sort(table(getBM$Biotype), decreasing = T)
#
# tb[9] <- sum(tb[9:47])
# tb <- tb[-(10:47)]
# names(tb)[9] <- "others"
# pie(tb, main = "Transcript biotype")


##################################################
# Data analysis. Transcript level
##################################################

#### Select samples
iCL <- 1:ncol(CCLE_tpm_log2) # all Diseases

iCL <- which(CCLE_sample_info$Subtype %in% c("AML"))

iCL <- which(CCLE_sample_info$Disease %in% c("lung"))
# Fernando yo quitaría ovario, endometrio y próstata que tienen un origen embrionario distinto
iCL <- which((CCLE_sample_info$Subtype %in% c("adenocarcinoma")) &
               !(CCLE_sample_info$Primary.Site %in% c("ovary", "prostate", "endometrium")))


#### Select transcripts

# Maximum number of transcripts
getBM <- getBM %>% group_by(Gene_name) %>% mutate(tr = n())
filtTr <- which(getBM$tr < 20)


# Select Transcripts by variance
vars <- rowMads(CCLE_tpm_log2[, iCL])
ord <- order(vars,decreasing = T)
iTr_ord <- ord[1:5000]
iTr <- intersect(filtTr, iTr_ord)

#### Select Ataris genes
elegidos <- (rowSums(CCLE_ataris[, iCL] > 0) > (ncol(CCLE_ataris[, iCL])/5)) &
                    (rowSums(CCLE_ataris[, iCL] < -1) > (length(iCL)/7))
ranges <- rowRanges(CCLE_ataris[,iCL])

iGn <- which((ranges[,1] < -2) & (ranges[,2] > 1) & elegidos)

rm(iTr_ord, elegidos, filtTr, ord, ranges, vars)

#### Correlation method
method <- "spearman"


##################################################
# Get cors
##################################################

# # wrong samples
# iCL <- setdiff(iCL, c(43, 136, 145, 143, 147))

cors <- corPv(CCLE_ataris = CCLE_ataris[iGn, iCL], 
              CCLE_tpm_log2 = CCLE_tpm_log2[iTr, iCL], 
              method = method)

iT <- match(cors$Transcript, getBM$Transcript_ID)
cors <- data.frame(cors, Transcript_name = getBM$Transcrip_name[iT], Biotype = getBM$Biotype[iT])
cors <- cors[,c(1,2,4,5,3)]
cors <- cors[order(abs(cors$cors),decreasing = T),]


# Number of transcripts
cors$nTr <- NA
iTr <- match(cors$Transcript, getBM$Transcript_ID)
cors$nTr <- getBM$tr[iTr]


##################################################
# Reverse Correlations
##################################################
cors$corTrGn <- NA

gn_R <- getBM$Gene_name[iTr]

rev_Ataris <- CCLE_ataris["RBBP7",]


for (s in 1:nrow(cors)){
  corSel <- cors[s,]
  tr <- as.character(corSel$Transcript_name)
  gene <- getBM$Gene_name[which(getBM$Transcrip_name == tr)]
  Trs <- which(getBM$Gene_name == gene)
  nTr <- length(Trs)
  cors$nTr[s] <- nTr
  a <- data.frame(CCLE_tpm_log2[Trs,iCL])
  if(ncol(a)==1){geneE <- log2((2^a-1)+1)
  }else{geneE <- log2(colSums(2^a-1)+1); cors$corTrGn[s] <- cor(t(a[corSel$Transcript,]), geneE, method = method)}
  ataris_gn <- corSel$Ataris_gn
  # cors$cor_gene[s] <- cor(geneE, CCLE_ataris[ataris_gn, iCL], method = method)
  # cors$pv_gene[s] <- cor.test(geneE, CCLE_ataris[ataris_gn, iCL], method = method)$p.value
  }

# cors$log10_tr_gn <- log10(cors$pv/cors$pv_gene)
cors <- cors[order(abs(cors$cors),decreasing = T),]
rownames(cors) <- NULL
# View(cors)

# length(unique(cors$Ataris_gn))
# length(unique(cors$Transcript_name))
# length(unique(unlist(strsplit(as.character(cors$Transcript_name), "-"))[c(T,F)]))

# cors <- cors[order(cors$adj.pv),]
###### plot Ataris

# pdf(paste0("./data/output/results/Transcripts-Ataris-LCLL_MM,spearman/",
#            sprintf("%s.Transcript_%s_Ataris_%s.pdf",j , info_tr$Transcrip_name, gn)),
#     width = 10, height = 5.7)
# write.csv(cors,paste0("./data/output/results/2017-06-05_Transcripts-Ataris-LCLL_MM,pearson,TRANSCRIPTS,mad/correlation.csv"))


##################################################
# Reverse correlations
##################################################

cors$Transcript_name <- as.character(cors$Transcript_name)
genesR <- getBM$Gene_name[match(cors$Transcript_name,getBM$Transcrip_name)]
isAt <- genesR %in% rownames(CCLE_ataris)

cors$isAt <- isAt

cors$corsRev <- 0

for (r in which(isAt)){

  corSel <- cors[r,]
  ataris_gn <- as.character(corSel$Ataris_gn)
  tr <- corSel$Transcript_name
  
  if(!ataris_gn %in% getBM$Gene_name) next
  gnR <- unlist(strsplit(tr,"[-]"))[c(T,F)]
  
  Trs <- which(getBM$Gene_name == ataris_gn)
  
  if (length(Trs) > 1){gnExp <- colSums(CCLE_tpm_log2[Trs, iCL])
  }else{gnExp <- CCLE_tpm_log2[Trs, iCL]}
  
  cors$corsRev[r] <- cor(CCLE_ataris[gnR, iCL], gnExp, method = method) 
  
}

cors$corsRevScore <- (rowProds(cbind(cors$cors, cors$corsRev)))




##################################################
# CancerDrivers and Drugs
##################################################

library(readr)
cancerDrivers <- read_delim("./data/input/cancerDrivers.csv", 
                            ";", escape_double = FALSE, trim_ws = TRUE,
                            skip = 2)

colnames(cancerDrivers)[1] <- "Ataris_gn"
cors <- merge(cors, cancerDrivers,by.x = "Ataris_gn", by.y = "Ataris_gn", all.x = T, all.y = F)

load("./data/output/Drugs.RData")
Drugs$Function <- tolower(Drugs$Function)

Drugs_inh <- Drugs[Drugs$Function %in% c("inhibitor","antagonist", "inhibitor"), ]
Drugs_inh <- Drugs_inh %>% group_by(Gene_name) %>% summarise(Drug = list(Drug_Name), Indication = list(Indication))

Drugs_inh$Indication <- mapply(Drugs_inh$Indication, FUN = function(X){(paste(unlist(X), collapse = ", "))})
Drugs_inh$Drug <- mapply(Drugs_inh$Drug, FUN = function(X){(paste(unlist(X), collapse = ", "))})

cors <- merge(cors, Drugs_inh, by.x = "Ataris_gn", by.y = "Gene_name", all.x = T, all.y = F)
cors <- cors[order(cors$adj.pv, decreasing = F),]
rownames(cors) <- NULL

cors <- cors[-which(cors$Category == "suppressor"), ]


##################################################
# Resnik
##################################################

# MIRAR UNPROT REGIONS!!!!

# 
load("./data/output/Pablo-CCLE/CCLE_tpm_gn_GxGO.RData")

gnN <- CCLE_tpm_gn$Gene_ID
CCLE_tpm_gn <- CCLE_tpm_gn[,-1]
rownames(CCLE_tpm_gn) <- gnN
rm(gnN)

method="spearman"

cors_All <- cor(t(CCLE_ataris[iGn, ]), t(log2(1+CCLE_tpm_gn)) , method = method)
plot(density(cors_All[lower.tri(cors_All)]))


source("../../aquilles/quasiresnikDistance.R")

resnik <- rep(NA, nrow(cors))

for (k in 1:nrow(cors)){
  tr <- cors$Transcript[k]
  gn <- as.character(cors$Ataris_gn[k])
  resnik[k] <- resnik_ataris(tr, gn, GeneGO = GxGO)
}

resnik_all <- quasiresnikDistanceallall(GxGO)
quantile(resnik_all, c(0.9, 0.99, 0.999))
         

cors$resnik <- resnik        

##################################################
# Create and write tables 
##################################################

# Table Genes
# corsWrite <- cors[,-6]
# colnames(corsWrite)[c(1,3,5)] <- c("Gene_Target_Ataris", "Marker", "Correlation")

cors$cors <- round(cors$cors, digits = 3)

tableGenes <- cors %>% group_by(Ataris_gn) %>% filter(adj.pv == min(adj.pv))
tableGenes <- tableGenes[order(tableGenes$adj.pv),]
rownames(tableGenes) <- NULL

# Table Transcripts
tableTranscripts <- cors[which(cors$corTrGn < 0.3), ]
tableTranscripts <- tableTranscripts[order(abs(tableTranscripts$corTrGn)),]
rownames(tableTranscripts) <- NULL

# Table Drugs
tableDrugs_leuk <- cors[grep("LEUK",cors$Indication), ]
rownames(tableDrugs_leuk) <- NULL

tableDrugs_other_aux <- cors[!is.na(cors$Drug), ]
tableDrugs_other_aux <- tableDrugs_other_aux[-grep("LEUK", tableDrugs_other_aux$Indication), ]
tableDrugs_other <- tableDrugs_other_aux %>% group_by(Ataris_gn) %>% filter(adj.pv == min(adj.pv))
rownames(tableDrugs_other) <- NULL

# Table SintheticLethal

tableSintheticLethal <- cors[which(cors$corsRev > 0.49), ]
tableSintheticLethal <- tableSintheticLethal[order(abs(tableSintheticLethal$corsRev), decreasing = T),]
rownames(tableSintheticLethal) <- NULL


tableResnik <- cors[which(cors$resnik > quantile(resnik_all, 0.99)), ]
tableResnik <- tableResnik[order(tableResnik$adj.pv),]
rownames(tableResnik) <- NULL


# write.csv(tableGenes, paste0("./data/output/results/2017-06-19_Transcripts-Ataris-LUNG,spearman,mad/tableGenes/tableGenes.csv"))
# write.csv(tableTranscripts, paste0("./data/output/results/2017-06-19_Transcripts-Ataris-LUNG,spearman,mad/tableTranscripts/tableTranscripts.csv"))
# write.csv(tableDrugs_lung, paste0("./data/output/results/2017-06-19_Transcripts-Ataris-LUNG,spearman,mad/tableDrugs_lung/tableDrugs_leuk.csv"))
# write.csv(tableDrugs_other, paste0("./data/output/results/2017-06-19_Transcripts-Ataris-LUNG,spearman,mad/tableDrugs_other/tableDrugs_other.csv"))
# write.csv(tableSintheticLethal, paste0("./data/output/results/2017-06-19_Transcripts-Ataris-LUNG,spearman,mad/tableSintheticLethal/tableSintheticLethal.csv"))
# write.csv(tableResnik, paste0("./data/output/results/2017-06-15_Transcripts-Ataris-LCLL_MM,spearman,TRANSCRIPTS,mad/tableResnik/tableResnik.csv"))
# 


##################################################
# Analysis of genes and transcripts
##################################################

########################################## plot transcripts
aux <- tableSintheticLethal

g <- 0
g <- g+1

# for(g in 2:10){
  tr <- aux$Transcript[g]
  gn <- as.character(aux$Ataris_gn[g])
  
  pA <- plotAtaris_transcripts(tr, gn, iCL, method = method, text = FALSE, subtype = TRUE)

  # pdf(paste0("./data/output/results/2017-06-15_Transcripts-Ataris-LCLL_MM,spearman,TRANSCRIPTS,mad/tableGenes/new/",
  #            sprintf("%s.Ataris_%s_Transcript_%s.pdf",g ,gn, aux$Transcript_name[g])),
  #     width = 12, height = 6.6)
  
  multiplot(plotlist = list(pA$gg1,pA$gg2, NULL, pA$gg3),cols=2)
  dev.off()
}

plotAtaris()

# leuk PLXNB2, CYP51A1

tr2 <- "ENST00000003100"
gn2 <- "PLXNB2"

pA <- plotAtaris_transcripts(tr2, gn2, iCL, method = method, text = FALSE, subtype = TRUE)
multiplot(plotlist = list(pA$gg1,pA$gg2, NULL, pA$gg3),cols=2)

########################################## 


g <- 0

g <- g+1


tr <- cors$Transcript[g]
gn <- as.character(cors$Ataris_gn[g])

pA <- plotAtaris_transcripts(tr, gn, iCL, method = method, text = FALSE, subtype = TRUE)
# grid.newpage(); grid.draw(rbind(ggplotGrob(pA$gg2), ggplotGrob(pA$gg3), size = "last"))

# pdf(paste0("./data/output/results/2017-06-05_Transcripts-Ataris-LCLL_MM,pearson,TRANSCRIPTS,mad/",
#            sprintf("%s.Transcript_%s_Ataris_%s.pdf",g , cors$Transcript_name[g], gn)),
#     width = 12, height = 6.6)
multiplot(plotlist = list(pA$gg1,pA$gg2, NULL, pA$gg3),cols=2)


# 
# cors <- cors[order(cors$corTrGn),]
# rownames(cors) <- NULL
# 
# for (g in 2:26){
#   tr <- cors$Transcript[g]
#   gn <- as.character(cors$Ataris_gn[g])
#   
#   pA <- plotAtaris_transcripts(tr, gn, iCL, method = method,n=NULL)
#   # grid.newpage(); grid.draw(rbind(ggplotGrob(pA$gg2), ggplotGrob(pA$gg3), size = "last"))
#   
#   pdf(paste0("./data/output/results/2017-06-05_Transcripts-Ataris-LCLL_MM,pearson,TRANSCRIPTS,mad/Transcript_Markers/",
#              sprintf("%s.Transcript_%s_Ataris_%s.pdf",g , cors$Transcript_name[g], gn)),
#       width = 12, height = 6.6)
#   multiplot(plotlist = list(pA$gg1,pA$gg2, NULL, pA$gg3),cols=2)
#   
#   dev.off()
# }


##################################################
# pheatmap of CL
##################################################

aux <- tableGenes

clN <- CCLE_sample_info$CCLE.name[iCL]
gnN <- as.character(unique(aux$Ataris_gn))
an <- CCLE_sample_info[iCL,c("Subtype")]

CLxGn_aux <- CCLE_ataris[gnN, iCL]
CLxGn <- (CLxGn_aux < -1) + 0

nCL <- rowSums(CLxGn)

annotation_cols <- data.frame(Subtype = an)
rownames(annotation_cols) <- colnames(CLxGn)

sel <- c(unique(as.character(aux$Ataris_gn)))[1:20]
pheatmap(CLxGn[sel, ],labels_col =  an, legend = F, annotation_col = annotation_cols, main = "Cell Lines with ATARiS score < -1",
         color = colorRampPalette((brewer.pal(n = 7, name = "BuGn")))(100),cluster_rows = T,
         cutree_rows = 4, cutree_cols = 3)


##################################################
# Lung
##################################################

pulmon <- which(CCLE_sample_info$Disease %in% c("lung"))

elegidos <- which((rowSums(CCLE_ataris>0) > (ncol(CCLE_ataris)/2)) & (rowSums(CCLE_ataris[, pulmon] < 0) > (length(pulmon)/2)) )

boxplot(t(CCLE_ataris[elegidos,]) ~ CCLE_sample_info$Site.Primary)


##################################################
# Alfonso Calvo TMPRSS4
##################################################

c <- corPv(CCLE_ataris = CCLE_ataris[5011:5012, iCL], 
      CCLE_tpm_log2 = CCLE_tpm_log2[iTr, iCL], 
      nCors = 1000, method = method)
c <- c[c$Ataris_gn == "TMPRSS4", ]
iT <- match(c$Transcript,getBM$Transcript_ID)
c <- data.frame(c, Transcript_name = getBM$Transcrip_name[iT])
c <- c[,c(1,2,6,3,4,5)]
View(c)

i <- 0
i <- i+1
plotAtaris(tr = c$Transcript[i],
           gn = c$Ataris_gn[i], iCL,
           method = method)


##################################################
# Mutation
##################################################

boxplot(CCLE_ataris["BRAF", ]~CCLE_sample_info$BRAF, main = "BRAF mut", ylab = "ATARiS Score", ylim = c(-3,2))
boxplot(CCLE_ataris["BRAF", iCL]~CCLE_sample_info$BRAF[iCL], main = "BRAF mut", ylab = "ATARiS Score", ylim = c(-3,2))

# La mutación en PIK3CA hace al gen PIK3CA más esencial en adenocarcinomas que en el resto
boxplot(CCLE_ataris["PIK3CA", ]~CCLE_sample_info$PIK3CA, main = "PIK3CA mut", ylab = "ATARiS Score", ylim = c(-4.5,2))
boxplot(CCLE_ataris["PIK3CA", iCL]~CCLE_sample_info$PIK3CA[iCL], main = "PIK3CA mut", ylab = "ATARiS Score", ylim = c(-4.5,2))

boxplot(CCLE_ataris["KRAS", ]~CCLE_sample_info$KRAS, main = "KRAS mut", ylab = "ATARiS Score", ylim = c(-4,2))
boxplot(CCLE_ataris["KRAS", iCL]~CCLE_sample_info$KRAS[iCL], main = "KRAS mut", ylab = "ATARiS Score", ylim = c(-4,2))


# Example: MET (LUAD). New therapeutic strategies
# "Exon skipping" that results in the elimination of a domain that 
# acts as a repressor of catalytic activity(Ma et al., 2003)
# 
# Tumors showing such omission of the exon in MET respond to therapies 
# directed against MET, originally developed to act against mutations that 
# affect the protein (Paik et al., 2015, Frampton et al 2015).
# 
# "ENST00000397752": doesn't respond to therapy (exon)
# "ENST00000422097": RESPONDS TO THERAPY (NO exon)


# iMet <- which(colnames(cors) %in% c("ENST00000397752","ENST00000422097"))
# View(cors[,iMet])
# colMaxs(cors[,iMet],na.rm = T)
# 
# iMetEx <- which(rownames(CCLE_tpm_log2) %in% c("ENST00000397752","ENST00000422097"))
# 
# plot(CCLE_tpm_log2[iMetEx[1],],
#      CCLE_ataris[which.max(cors[,iMet]),-1],
#      xlab = "log2 of expression (TPM)",
#      ylab = "ATARiS Score",
#      main = "MET ENST00000397752")
# 
# plot(CCLE_tpm_log2[iMetEx[2],],
#      CCLE_ataris[which.max(cors[,iMet]),-1],
#      xlab = "log2 of expression (TPM)",
#      ylab = "ATARiS Score",
#      main = "MET ENST00000422097")
# 
# plot(CCLE_tpm_log2[iMetEx[2],] - CCLE_tpm_log2[iMetEx[1],],
#      CCLE_ataris[which.max(cors[,iMet]),-1],
#      xlab = "log2 FC two isoforms",
#      ylab = "ATARiS Score",
#      main = "log2 FC two isoforms")
# 
# matplot((CCLE_tpm_log2[7,]),type="l")


##################################################
# Mutation with BayesFactors
##################################################


load(file="./data/output/preparingData_BayesFactors.RData")

boxplot(BayesFactors["BRAF", ]~CCLE_sample_info$BRAF, main = "BRAF mut", ylab = "Bayes Factor")

# La mutación en PIK3CA hace al gen PIK3CA más esencial en adenocarcinomas que en el resto
boxplot(BayesFactors["PIK3CA", ]~CCLE_sample_info$PIK3CA, main = "PIK3CA mut", ylab = "Bayes Factor")

boxplot(BayesFactors["KRAS", ]~CCLE_sample_info$KRAS, main = "KRAS mut", ylab = "Bayes Factor")


##################################################
# Arkaitz (run 1) 2)
##################################################

arkaitz_CL <- c("PC3", "DU145", "22RV1", "VCAP")
i_ar <- match(arkaitz_CL, CCLE_sample_info_ALL$Cell_Line)

gene <- "TRIB1"
Trs <- which(getBM$Gene_name == gene)
trsNm <- getBM$Transcript_ID[Trs]
  
a <- CCLE_tpm_ALL[Trs,i_ar+1]
psi <- t(t(a)/colSums(a))

ggmatplot(dat = t(a), xnames = TRUE, highlight = trsNm) +   
  ylab("tpm") + 
  xlab("Samples") + ggtitle("Expression: transcripts of TRIB1")


ggmatplot(dat = t(psi), xnames = TRUE, highlight = trsNm) +   
  ylab("PSI") + 
  xlab("Samples") + ggtitle("PSI of TRIB1")
