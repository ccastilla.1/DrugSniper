library(dplyr)
library(readr)
library(Matrix)
library(matrixStats)
library(psych)
library(tidyr)
library(ggplot2)
library(pheatmap)
library(RColorBrewer)
library(progress)
library(biomaRt)
library(grid)
library(qvalue)
library(impute)
library(WriteXLS)
source("../../code/ggmatplot.R")
source("./code/auxFunctions.R")
source("../../code/multiplot.R")
source("../../code/Wilcoxon.z.matrix.R")
source("../../code/progressTime.R")
source("../../code/GetFDR.R")
source("../../code/Wilcoxon.z.matrix.R")
source("./code/boxplotMutation.R")
source("./code/boxplotEssentiality.R")
source("./code/getPairs_mut.R")
source("./code/predictBiomarkers_mut.R")
source("./code_lu/F_EG_Selection.R")
source("./code_lu/F_predictBiomarkers_trans.R")
source("./code/getPairs_tr.R")
source("Z:/BACKUP_GRUPO/FernandoCarazo/ProyectosCompletos/hnRNP_Huelga_Analysis/GT_analysis/GT_analysis3.R")
source("./code/F_get_exp_genes.R")
source("./code/F_filter_tr_gn_and_getGN.R")
source("./code/getPairs_tr2.R")
source("./shinyApp/transcriptachilles/fun-ggmatplot_ModifySource.R")

load("./transcriptachilles/dataShiny/DataForShiny2_opt.RData")
pheatmap((CCLE_tpm_log2[1:30,1:10]), 
         labels_row = getBM$Transcrip_name[1:30], 
         # labels_col = CCLE_sample_info$Cell_Line[1:10],
         main = "Transcript expression. log2(tpm)", cluster_rows = T, cluster_cols = F)


pheatmap((CCLE_ataris[1:30,1:10]), 
         # labels_row = getBM$Transcrip_name[1:30], 
         labels_col = CCLE_sample_info$Cell_Line[1:10],
         main = "ATARiS Score", cluster_rows = T, cluster_cols = F)

h <- 1
ggmatplot(CCLE_tpm_log2[1:30,h]) + 
  ylab("log2(tpm)") + xlab("30 random genes") + 
  ggtitle(sprintf("Expression of 30 transcripts of Cell Line  %s", CCLE_sample_info$Cell_Line[h]))

# Figures

# Gene 1
Isoform_1.1 <- rep(c(1,3), c(5,5))
Isoform_1.2 <- rep(c(2,4), c(5,5))
Isoform_1.3 <- rep(c(2.5,4.5), c(5,5))
Gene_1 <- Isoform_1.1 + Isoform_1.2 + Isoform_1.3

M1 <- data.frame(Gene_1, Isoform_1.1, Isoform_1.2, Isoform_1.3)
rownames(M1) <- paste("Sample", rep(1:10))

set.seed(999)
M1r <- M1 + matrix(runif(nrow(M1) * ncol(M1)), nrow = nrow(M1), ncol = ncol(M1))

x11(); ggmatplot(as.matrix(M1r), highlight = "Gene_1") + 
  ylab("Expression") + xlab("Samples") + ggtitle(label = "Gene 1") +
  theme(plot.title = element_text(hjust = 0.5)) + ylim(c(0,max(M1r) + 0.5)) + 
  geom_vline(xintercept = 5.5, lty = 2, col = 4)


# Gene 2

Isoform_2.1 <- rep(c(1,5), c(5,5))
Isoform_2.2 <- rep(c(5,1), c(5,5))
Isoform_2.3 <- rep(c(2.5), 10)
Gene_2 <- Isoform_2.1 + Isoform_2.2 + Isoform_2.3

M2 <- data.frame(Gene_2, Isoform_2.1, Isoform_2.2, Isoform_2.3)
rownames(M2) <- paste("Sample", rep(1:10))

set.seed(999)
M2r <- M2 + matrix(runif(nrow(M2) * ncol(M2)), nrow = nrow(M2), ncol = ncol(M2))

x11(); ggmatplot(as.matrix(M2r), highlight = "Gene_2") + 
  ylab("Expression") + xlab("Samples") + ggtitle(label = "Gene 2")+
  theme(plot.title = element_text(hjust = 0.5)) + ylim(c(0,max(M2r) + 0.5))+ 
  geom_vline(xintercept = 5.5, lty = 2, col = 4)


# Gene 3

Isoform_3.1 <- rep(c(1,7), c(5,5))
Isoform_3.2 <- rep(c(5,4), c(5,5))
Isoform_3.3 <- rep(c(3,2.5), c(5,5))
Isoform_3.4 <- rep(c(2,1.5), c(5,5))

Gene_3 <- Isoform_3.1 + Isoform_3.2 + Isoform_3.3 + Isoform_3.4 + Isoform_3.5

M3 <- data.frame(Gene_3, Isoform_3.1, Isoform_3.2, Isoform_3.3, Isoform_3.4, Isoform_3.5)
rownames(M3) <- paste("Sample", rep(1:10))

set.seed(999)
M3r <- M3 + matrix(runif(nrow(M3) * ncol(M3)), nrow = nrow(M3), ncol = ncol(M3))

x11(); ggmatplot(as.matrix(M3r), highlight = "Gene_3") + 
  ylab("Expression") + xlab("Samples") + ggtitle(label = "Gene 3")+
  theme(plot.title = element_text(hjust = 0.5)) + ylim(c(0,max(M3r) + 0.5))+ 
  geom_vline(xintercept = 5.5, lty = 2, col = 4)




# Kidney ------------------------------------------------------------------

load("./code/transcriptachilles/dataShiny/DataForShiny2.RData")
iCL <- which(CCLE_sample_info_match$Primary.Site == "kidney")

p <- plot_transcripts_essential2(tr = "ENST00000511975",
                                 gn = "PER3", getBM = getBM,
                                 CCLE_ataris = CCLE_demeter_match,
                                 CCLE_tpm_log2 = log2(1+CCLE_tpm_match),
                                 CCLE_sample_info = CCLE_sample_info_match,
                                 iCL = iCL, text = F, subtype = F)

x11();p$gg3

df <- data.frame(log2_tpm = log2(1+CCLE_tpm_match["ENST00000511975",iCL]), 
                 Essential_PER3 = CCLE_demeter_match["PER3", iCL] < -2)
x11();ggplot(df, aes(Essential_PER3, log2_tpm)) + 
  geom_boxplot(aes(color = Essential_PER3)) + 
  geom_jitter(aes(color = Essential_PER3)) +
  ggtitle("Transcript expression: SEC31A-020")



# Different tests ---------------------------------------------------------

# Gene 1: cor perfects

G1 <- data.frame(Essentiality = seq(-3, 1.5, length.out = 10),
                 Espression = seq(0, 10, length.out = 10))
rownames(G1) <-  paste("Sample", rep(1:10))

noise <- function(x, div = 1){
  set.seed(999)
  x2 <- x + matrix(runif(nrow(x) * ncol(x))/div, nrow = nrow(x), ncol = ncol(x))
  x2 <- as.data.frame(x2)
  return(x2)
  
}

G1_r <- noise(G1,2)

G1_r$Essential <- as.factor(G1_r$Essentiality <= -2)


x11(); ggplot(G1_r, aes(Espression, Essentiality)) + geom_point(aes(color = Essential), size = 2) + ggtitle("Case 1: Bad")

cor(G1_r$Essentiality, G1_r$Espression) # 0.9937941
cor.test(G1_r$Essentiality, G1_r$Espression) # p-value = 6.441e-09

wilcox.test(G1_r$Espression ~ G1_r$Essential) # p-value = 0.04444

Dmatrix = model.matrix(~ G1_r$Essential)
Cmatrix = t(t(c(0,1)))

Fit<-lmFit(G1_r$Espression, Dmatrix)
Fit<-contrasts.fit(Fit, Cmatrix)
Fit <- eBayes(Fit)
toptable(Fit, number = Inf) #  0.02464215

                                           

# Gene 2: sep perfect low

set.seed(4)
G1 <- data.frame(Essentiality = c(runif(5, min = -3, max = -2.1), runif(5, min = -2, max = 0)),
                 Espression = c(runif(5, min = 2.5, max = 3), runif(5, min = 0, max = 2.4)))
rownames(G1) <-  paste("Sample", rep(1:10))

G1_r <- G1

G1_r$Essential <- as.factor(G1_r$Essentiality <= -2)


x11();ggplot(G1_r, aes(Espression, Essentiality)) + geom_point(aes(color = Essential), size = 2) + ggtitle("Case 2. Medium")

cor(G1_r$Essentiality, G1_r$Espression) # -0.5717623
cor.test(G1_r$Essentiality, G1_r$Espression) # p-value = 0.08419

wilcox.test(G1_r$Espression ~ G1_r$Essential) # p-value = 0.007937

Dmatrix = model.matrix(~ G1_r$Essential)
Cmatrix = t(t(c(0,1)))

Fit<-lmFit(G1_r$Espression, Dmatrix)
Fit<-contrasts.fit(Fit, Cmatrix)
Fit <- eBayes(Fit)
toptable(Fit, number = Inf) #  0.005796645  



# Gene 3: sep perfect high

set.seed(4)
G1 <- data.frame(Essentiality = c(runif(5, min = -3, max = -2.1), runif(5, min = -2, max = 0)),
                 Espression = c(runif(5, min = 10, max = 15), runif(5, min = 0, max = 0.5)))
rownames(G1) <-  paste("Sample", rep(1:10))

G1_r <- G1

G1_r$Essential <- as.factor(G1_r$Essentiality <= -2)


x11(); ggplot(G1_r, aes(Espression, Essentiality)) + geom_point(aes(color = Essential), size = 2) + ggtitle("Case 3. Good")

cor(G1_r$Essentiality, G1_r$Espression) #  -0.8411886
cor.test(G1_r$Essentiality, G1_r$Espression) # p-value = 0.002287

wilcox.test(G1_r$Espression ~ G1_r$Essential) # p-value = 0.007937

Dmatrix = model.matrix(~ G1_r$Essential)
Cmatrix = t(t(c(0,1)))

Fit<-lmFit(G1_r$Espression, Dmatrix)
Fit<-contrasts.fit(Fit, Cmatrix)
Fit <- eBayes(Fit)
toptable(Fit, number = Inf) #  2.88182e-07


# Case study --------------------------------------------------------------

load("./shinyApp/transcriptachilles/dataShiny/DataForShiny2_opt.RData")

iCL <- which(CCLE_sample_info_match$Primary.Site == "kidney")

p <- plot_transcripts_essential2(tr = "ENST00000544786",
                                 gn = "IRAK1", getBM = getBM,
                                 CCLE_ataris = CCLE_demeter_match,
                                 CCLE_tpm_log2 = log2(1+CCLE_tpm_match),
                                 CCLE_sample_info = CCLE_sample_info_match,
                                 iCL = iCL, text = F, subtype = F)

x11();p$gg3
x11();p$gg2
x11();p$gg1

df <- data.frame(log2_tpm = log2(1+CCLE_tpm_match["ENST00000511975",iCL]), 
                 Essential_PER3 = CCLE_demeter_match["PER3", iCL] < -2)
x11();ggplot(df, aes(Essential_PER3, log2_tpm)) + 
  geom_boxplot(aes(color = Essential_PER3)) + 
  geom_jitter(aes(color = Essential_PER3)) +
  ggtitle("Transcript expression: SEC31A-020")



