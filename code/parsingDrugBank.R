library(Matrix)
library(dplyr)
load("./data/output/preparingData.RData")

# DRUGBANK 3765 relations, 958 molecules, 877 genes
DrugBank_molecule <- read.delim("./data/input/DrugBank/DrugBank_molecule_2017_06_09.txt",stringsAsFactors = F, sep=",")
DrugBank_to_target <- read.delim("./data/input/DrugBank/DrugBank_to_target_ensembl_2017_06_09.txt",stringsAsFactors = F, sep=",")
colnames(DrugBank_to_target) <- c("DrugBank_ID", "Ensembl_gene_ID", "Function")
# barplot(table(DrugBank_to_target$Function)[order(table(DrugBank_to_target$Function), decreasing = T)], las=2)
DrugBank_aux <- merge(DrugBank_molecule[,c(1,2)], DrugBank_to_target, by.x = "DrugBank_ID", by.y = "DrugBank_ID")
colnames(DrugBank_aux) <- c("Drug_ID", "Drug_Name", "Ensembl_gene_ID", "Function")
DrugBank_aux$Indication <- NA

########################################### chembl ######################
# Approved_drug
CH_targets <- read.delim("./data/input/ChEMBL/Approved/Approved_drug_targets.txt",stringsAsFactors = F, sep=",")
CH_indications <- read.delim("./data/input/ChEMBL/Approved/Approved_drug_indications.txt",stringsAsFactors = F, sep=",")
CH_targets <- CH_targets[!duplicated(CH_targets),]
CH_indications <- CH_indications[!duplicated(CH_indications),]
CH_indications <- CH_indications %>% group_by(Drug_ID) %>% summarize(Indications = list(Indication))
chembl <- merge(CH_targets, CH_indications, by = "Drug_ID", all = T)
iCh <- chembl$Drug_ID_Chembl %in% DrugBank
colnames(chembl) <- c("Drug_ID", "Drug_Name", "Ensembl_gene_ID", "Function", "Indication")

# Merge
drugs <- rbind(DrugBank_aux, chembl)


gn <- getBM[,c("Gene_ID", "Gene_name")]; gn <- gn[!duplicated(gn),]

Drugs <- merge(gn, drugs,  by.x = "Gene_ID", by.y = "Ensembl_gene_ID", all.y = T)
Drugs <- Drugs[,c(3,4,1,2,5,6)]

# iF <- which(DrugBank$Function == "inhibitor")
# DrugBank_sel <- DrugBank[iF,]
# drugs <- as.factor(DrugBank_sel$DrugBank_ID)
# genes <-  as.factor(DrugBank_sel$Gene_name)
# 
# DxG_inh <- sparseMatrix(i = as.numeric(drugs), j = as.numeric(genes),x = 1)
# rownames(DxG_inh) <- levels(drugs)
# colnames(DxG_inh) <- levels(genes)
# DxG_inh[1:5,1:5]

# save(Drugs, file = "./data/output/Drugs.RData")

