library(dplyr)
library(readr)
library(Matrix)
library(matrixStats)
library(psych)
library(biomaRt)

# Load CCLE expression data 
load(file = "D:\\Users\\fcarazo\\Documents\\Bunker\\Samples\\CCLE-Kallisto\\CCLE_tpm_all.RData")
CCLE_tpm_ALL <- CCLE_tpm; rm(CCLE_tpm)

# Load annotations
load(file = "D:\\Users\\fcarazo\\Documents\\Bunker\\Samples\\CCLE-Kallisto\\CCLE_sample_info_all.RData")

###################################################

CCLE_sample_info$Subtype[grep("chronic_myeloid_leukaemia",CCLE_sample_info$Subtype)] <- "CML"
CCLE_sample_info$Subtype[grep("acute_lymphoblastic_B_cell_leukaemia",CCLE_sample_info$Subtype)] <- "ALL"
CCLE_sample_info$Subtype[grep("acute_myeloid_leukaemia",CCLE_sample_info$Subtype)] <- "AML"
CCLE_sample_info$Subtype[grep("plasma_cell_myeloma",CCLE_sample_info$Subtype)] <- "MM"
CCLE_sample_info$Subtype[grep("plasma_cell_myeloma",CCLE_sample_info$Subtype)] <- "MM"

CCLE_sample_info_rd <- CCLE_sample_info

colnames(CCLE_sample_info)[4] <- "Primary.Site"

xAguirre <- read.csv("./data/input/leukemia-Xabi/CCLE_leukemia_Cell_Lines_27-7-17.csv", sep=";")
CCLE_sample_info <- merge(CCLE_sample_info, xAguirre, by="Cell_Line", all.x = T)
rm(xAguirre)

CCLE_tpm_all <- CCLE_tpm
CCLE_sample_info_all

# ###################################################
# 1) Ataris Score
###################################################

CCLE_ataris <- read.delim("./data/input/ATARiS_Achilles_v2.4.3.csv",
                          sep = ";", dec = ",")

dim(CCLE_ataris) #5711  217

colnames(CCLE_ataris) <- toupper(as.character(colnames(CCLE_ataris)))


iA <- match(colnames(CCLE_tpm_ALL),colnames(CCLE_ataris))
iT_A <- which(!is.na(iA))
iA <- iA[!is.na(iA)]
CCLE_ataris <- CCLE_ataris[,c(1,iA)]
dim(CCLE_ataris) #5711  172
rm(iA, iT_A, CCLE_sample_info_rd)

# CCLE_tpm <- CCLE_tpm_ALL[,c(1,iT_A)]
# CCLE_sample_info <- CCLE_sample_info[(iT_A-1),]
# rm(iT_A)
# # Make syntactically valid names out of character vectors.
# rownames(CCLE_ataris)<- make.names(CCLE_ataris[,1],unique = T)
# 
# CCLE_ataris <- as.matrix(CCLE_ataris[,-1])

# save(CCLE_sample_info, file = "D:\\Users\\fcarazo\\Documents\\Bunker\\Samples\\CCLE-Kallisto\\CCLE_sample_info_CCLE_ATARiS.RData")
# write.csv2(CCLE_sample_info,  file = "D:\\Users\\fcarazo\\Documents\\Bunker\\Samples\\CCLE-Kallisto\\CCLE_sample_info_CCLE_ATARiS.csv")


# save.image("./data/output/preparingData3.RData")
# load("./data/output/preparingData3.RData")


###################################################
# 2) Bayes Factors
###################################################

load("../../aquilles/BayesFactors.RData")

dim(BayesFactors) #11527   216

colnames(BayesFactors) <- toupper(as.character(colnames(BayesFactors)))

iA <- match(CCLE_sample_info$Cell_Line, as.character(colnames(BayesFactors)))
iT_A <- which(!is.na(iA))
iA <- iA[!is.na(iA)]
BayesFactors <- BayesFactors[,iA]
dim(BayesFactors) #11527  176
colnames(BayesFactors) <- CCLE_sample_info$CCLE.name[iT_A]

CCLE_bayesFactor <- BayesFactors
rm(BayesFactors, iA, iT_A)

# CCLE_tpm <- CCLE_tpm_ALL[,c(1,iT_A+1)]
# CCLE_sample_info <- CCLE_sample_info[(iT_A),]
# rm(iT_A)
# Make syntactically valid names out of character vectors.

CCLE_sample_info_ALL <- CCLE_sample_info


iI <- match(CCLE_sample_info_ALL$CCLE.name, colnames(CCLE_tpm_ALL)[-1])
CCLE_tpm_ALL <- CCLE_tpm_ALL[,c(1,iI+1)]

rm(CCLE_sample_info, iI)

# write.csv2(CCLE_sample_info_ALL, file = "D:/Users/fcarazo/Documents/Bunker/Samples/CCLE-Kallisto/CCLE_sample_info_all.csv")

###################################################
# 2) DEMETER
###################################################
# 
# CCLE_demeter_sample_info <- read.csv("./data/input/DEMETER/TableS1_SampleInfo.csv", sep = ";")
# 
# ExpandedGeneZSolsCleaned <- read_csv("./data/input/DEMETER/ExpandedGeneZSolsCleaned.csv")
# ExpandedGeneZSolsCleaned <- as.data.frame(ExpandedGeneZSolsCleaned)
# rownames(ExpandedGeneZSolsCleaned) <- ExpandedGeneZSolsCleaned[,1]
# ExpandedGeneZSolsCleaned <- as.matrix(ExpandedGeneZSolsCleaned[,-1])
# library(impute)
# Salida <- impute.knn(as.matrix(ExpandedGeneZSolsCleaned) ,k = 10, rowmax = 0.75, colmax = 0.8, maxp = 1500, rng.seed=362436069)
# CCLE_demeter <- Salida$data
# 
# tissue <-  as.factor(unlist(lapply(strsplit(colnames(CCLE_demeter),"_"), function(x) {paste(x[-1],sep="",collapse="_")})))
# # SVD <- svd(CCLE_demeter)
# # boxplot(SVD$v[,1] ~ tissue)
# 
# 
# iD <- match(CCLE_sample_info_ALL$CCLE.name, as.character(colnames(CCLE_demeter)))
# iT_D <- which(!is.na(iD))
# iD <- iD[!is.na(iD)]
# CCLE_demeter <- CCLE_demeter[,iD]
# dim(CCLE_demeter) #11527  176

# save.image(file="./data/output/preparingData5.RData")
