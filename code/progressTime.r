# Usage
# st <- Sys.time()
# progressTime(st)


progressTime <- function(T.start, T.end=0){
  if(T.end==0) {T.end <- Sys.time()}
  T.end - T.start
  cat("Summary\n")
  cat("\tStart:\t", format(T.start), "\n")
  cat("\tEnd:\t", format(T.end), "\n\t")
  print(T.end - T.start)

}

PB <- function(i){
  require(progress)
  pb <- progress_bar$new(
    format = "  downloading [:bar] :percent progress: :eta",
    total = i, clear = FALSE, width= 80)
  return(pb)
}
