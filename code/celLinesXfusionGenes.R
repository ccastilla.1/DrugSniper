library(readxl)
library(Matrix)
Achilles <- read_excel("Z:/BACKUP_GRUPO/FernandoCarazo/ProyectosCompletos/Achilles-AS/data/input/CCLE_with_Achilles_v2.4.3.xls",
                       sheet = "Achilles")

FG <- read_excel("Z:/BACKUP_GRUPO/FernandoCarazo/ProyectosCompletos/Achilles-AS/data/input/CCLE_with_Achilles_v2.4.3.xls",
                       sheet = "S7-Gene Fusions")
FG <- FG[,c(1,23)]
colnames(FG) <- FG[1,]
FG <- FG[-1,]

i <- FG$`Cell line` %in% Achilles$Name
FGa <- FG[i,]

ls <- setdiff(FGa$`Cell line`,CL$`Cell line`)

cl <- as.factor(FGa$`Cell line`)
fg <- as.factor(FGa$FG)

CxF <- sparseMatrix(i = as.numeric(cl),j = as.numeric(fg),x = 1)
rownames(CxF) <- levels(cl)
colnames(CxF) <- levels(fg)

CxF <- as.matrix(CxF)
write.csv(CxF,"./data/output/celLinesXfusionGenes.csv")
