library(readr)
library(dplyr)
library(tidyr)
options(java.parameters = "- Xmx4024m"); library(ontoCAT)

########################################
# BrendaTissue
########################################
# BrendaTissue <- getOntology(pathToURI = "http://data.bioontology.org/ontologies/BTO/submissions/33/download?apikey=8b5b7825-538d-40e0-9e9e-5ab9274a9aeb")

BrendaTissue <- getOntology(pathToURI = "./data/input/ontologies/BrendaTissues/BrendaTissue.obo")

aux <- getAllTerms(BrendaTissue)

accession <- mapply(aux, FUN = function(x){getAccession(x)}); accession <- as.character(accession)
name <- mapply(aux, FUN = function(x){getLabel(x)}); name <- as.character(name)
terms <- data.frame(Accession = accession, Name = name,stringsAsFactors = F)
rm(accession, name)

iN <- terms$Accession[grep("A-549",terms$Name)]
getAllTermParentsById(BrendaTissue,iN)

annotations <- rep(NA, nrow(terms))

for(t in 1:nrow(terms)){
  cat(t,"\n")
  aux <- getAllTermParentsById(BrendaTissue,terms$Accession[t])
  annotations[t] <- paste(mapply(aux, FUN = function(x){paste(getLabel(x))}), collapse = ", ")
}

terms$Annotation <- annotations

# save(terms, file = "./data/input/ontologies/BrendaTissues/BrendaTissue.RData")
load("./data/input/ontologies/BrendaTissues/BrendaTissue.RData")

########################################
# Prueba
########################################

# cellosaurus <- getOntology(pathToURI = "./data/input/ontologies/Cellosaurus/cellosaurus.obo")
# 
# getAllTerms(cellosaurus)

########################################
# Cellosaurus
########################################

# ---------  ---------------------------     ----------------------
#   Line code  Content                         Occurrence in an entry
# ---------  ---------------------------     ----------------------
#   ID         Identifier (cell line name)     Once; starts an entry
# AC         Accession (CVCL_xxxx)           Once
# AS         Secondary accession number(s)   Optional; once
# SY         Synonyms                        Optional; once
# DR         Cross-references                Optional; once or more
# RX         References identifiers          Optional: once or more
# WW         Web pages                       Optional; once or more
# CC         Comments                        Optional; once or more
# ST         STR profile data                Optional; once or more
# DI         Diseases                        Optional; once or more
# OX         Species of origin               Once or more
# HI         Hierarchy                       Optional; once or more
# OI         Originate from same individual  Optional; once or more
# SX         Sex (gender) of cell            Optional; once
# CA         Category                        Once
# //         Terminator                      Once; ends an entry


#ftp://ftp.expasy.org/databases/cellosaurus
cellosaurus <- read_csv("./data/input/ontologies/Cellosaurus/cellosaurus.txt", 
                        col_names = FALSE, skip = 49)
cellosaurus <- as.data.frame(cellosaurus)
sep <- which(cellosaurus=="//")
cl <- matrix(unlist(strsplit(x = cellosaurus[-sep,], split = "  ")), ncol = 2, byrow = T)
cl <- as.data.frame(cl); colnames(cl) <- c("Label", "Value")

st <- c(1,sep[-length(sep)]+1)
nd <- sep-1
tb <- cbind(st,nd)
times <- (tb[,2]-tb[,1])+1
ids <- as.character(cl$Value[which(cl$Label == "ID")])

cl$ids <- rep(ids, times)
cl <- cl[!duplicated(cl),]

nHo <- cl$ids[grep("Homo sapiens", cl$Value)]

cl <- cl[cl$ids %in% nHo,]
cl <- cl[-which(cl$Label=="OX"), ]
cl <- cl[-which(cl$Label=="WW"), ]
cl$Label <- as.character(cl$Label)
cl$Label[grep("BTO",cl$Value)] <- "BTO"

cl <- cl[-which(cl$Label=="DR"), ]

cl_DI <- cl[which(cl$Label=="DI"), ]
rownames(cl_DI) <- 1:nrow(cl_DI)
cl_DI$Value <- as.character(cl_DI$Value)
aux <- cl_DI %>% group_by(Label, ids) %>% summarise(Value=list(Value))
aux$Value <- mapply(aux$Value , FUN = function(X){(paste(unlist(X), collapse = ", "))})
aux <- aux[,c(1,3,2)]

aux2 <- bind_rows(cl[-which(cl$Label=="DI"), ], aux)

cl <- aux2

iR <- which(cl$Label=="DI" | cl$Label=="SY" | cl$Label=="BTO")

aux <- cl[iR, ]
aux <- aux[-which(duplicated(aux[,c(1,3)])), ]
rownames(aux) <- NULL
aux2 <- aux %>% spread(key = Label, value = Value)

aux2 <- aux2[(!is.na(aux2$DI) & !is.na(aux2$SY)), ]

colnames(aux2) <- c("Cell_Line_ID","BTO", "Disease", "Synonyms")

cellosaurus <-  aux2

# save(cellosaurus, file = "./data/input/ontologies/Cellosaurus/Cellosaurus.RData")
load(file = "./data/input/ontologies/Cellosaurus/Cellosaurus.RData")

########################################
# Merge data
########################################
# clear all

load("./data/input/ontologies/BrendaTissues/BrendaTissue.RData")
colnames(terms)[3] <- "BrendaTissue_An"
terms <- terms[-which(terms$BrendaTissue_An == ""),]

load(file = "./data/input/ontologies/Cellosaurus/Cellosaurus.RData")
cellosaurus$Synonyms <- as.character(cellosaurus$Synonyms)
cellosaurus$Cell_Line_ID <- gsub(" ", "", cellosaurus$Cell_Line_ID)
rownames(cellosaurus) <- NULL

cl_an <- rep(NA, nrow(cellosaurus))

cellosaurus$BTO <- gsub(":", "_", cellosaurus$BTO)
cellosaurus$BTO <- gsub("BTO; ", "", cellosaurus$BTO)
cellosaurus$BTO <- gsub(" ", "", cellosaurus$BTO)


for(t in 1:nrow(cellosaurus)){ #
  cat(t, "\n")
  gr <- grep(cellosaurus$BTO[t], terms$Accession)[1]
  cl_an[t] <- terms$BrendaTissue_An[gr]
  
}

cellosaurus$BrendaTissue_An <- cl_an

CL_Ontology <- cellosaurus

# save(CL_Ontology, file = "./data/input/ontologies/CL_Ontology.RData")

load(file = "Z:/BACKUP_GRUPO/FernandoCarazo/ProyectosCompletos/Achilles-CCLE-TCGA/Achilles-AS/data/input/ontologies/ontologies.RData")



########################################
# Merge data - CCLE
########################################
load("~/Bunker/Samples/CCLE-Kallisto/CCLE_sample_info_all.RData")
load(file = "./data/input/ontologies/ontologies.RData")

CL_Ontology$Cell_Line_ID <- paste(CL_Ontology$Cell_Line_ID, CL_Ontology$Synonyms, sep=";")

GR <- rep(NA, nrow(CCLE_sample_info))
nGR <- rep(NA, nrow(CCLE_sample_info))

for(c in 1:nrow(CCLE_sample_info)){
  gr <- grep(CCLE_sample_info$Cell_Line[c], CL_Ontology$Synonyms)
  GR[c] <- paste(gr, collapse=", ")
  nGR[c] <- length(gr)
  
}

rownames(CCLE_sample_info) <- 1:nrow(CCLE_sample_info)

CCLE_sample_info$GR <- GR
CCLE_sample_info$nGR <- nGR

r <- 0

# write.csv(CCLE_sample_info, file = "./data/input/ontologies/CCLE_sample_info.csv")

r <- r+1
View(CL_Ontology[as.numeric(CCLE_sample_info$GR[r]),])


### Pancreas ###
iCL <- which(CCLE_sample_info$Primary.Site == "pancreas")

View(CCLE_sample_info[iCL,])
n <- 0

n <- n+1
CL_Ontology[ CCLE_sample_info$GR[iCL[n]],4:5]
  