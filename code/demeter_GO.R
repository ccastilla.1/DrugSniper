source("../../code/Wilcoxon.z.matrix.R")
source("../../code/progressTime.R")
library(biomaRt)
library(pheatmap)
library(limma)

# Set the version of the genome that you want to work with
HOST <- "mar2016.archive.ensembl.org" #Ensembl GRCh38.84
mart <- useMart(host = HOST, biomart = "ENSEMBL_MART_ENSEMBL", dataset =   "hsapiens_gene_ensembl") # hsapiens for human
mart<-useDataset("hsapiens_gene_ensembl",mart=mart) #Homo sapiens genes (GRCh37) Ensembl v.74

# Get info from biomaRt
BIOMART_rd <- getBM(attributes=c("ensembl_gene_id", "hgnc_symbol", "go_id", "name_1006"),mart=mart)

# Deletting rows with any missing values
imiss <- which(BIOMART_rd$go_id=="" | BIOMART_rd$name_1006=="")
BIOMART <- BIOMART_rd[-imiss,]
biomatInfo <- unique(data.frame(gene_id = BIOMART$go_id, go_id = BIOMART$name_1006))

############################################################
# Extracting the information into matrices
############################################################
GO <- unique(data.frame(gene_id = BIOMART$hgnc_symbol, go_id = BIOMART$name_1006))

genes_go <- as.factor(GO$gene_id)
go_id <- as.factor(GO$go_id)
GxO <- sparseMatrix(as.numeric(genes_go), as.numeric(go_id),x=1)
rownames(GxO) <- levels(genes_go)
colnames(GxO) <- levels(go_id)
dim(GxO) # 

# cleaning
nGn <- colSums(GxO)
keep <- which(nGn > 10 & nGn < 200)
GxO <- GxO[,keep]


iM <- match(rownames(CCLE_demeter), rownames(GxO))
CCLE_demeter_GOmatch <- CCLE_demeter[!is.na(iM), ]
iM <- iM[!is.na(iM)]
GxO <- GxO[iM, ]
identical(rownames(CCLE_demeter_GOmatch), rownames(GxO))
dim(GxO)


Wx <- t(Wilcoxon.z.matrix(ExprT = t(CCLE_demeter_GOmatch), GeneGO = GxO))


# tissue <-  as.factor(unlist(lapply(strsplit(colnames(Wx),"_"), function(x) {paste(x[-1],sep="",collapse="_")})))
identical(CCLE_demeter_sample_info$Name, colnames(Wx))
isAML <- grepl("AML", CCLE_demeter_sample_info$Subtype) * 1

Dmatrix <- data.frame(Intercept = 1, isAML = isAML)

Cmatrix <- t(t(c(0,1)))
Wx[Wx>3] <- 3
Wx[Wx < -3] <- -3

lm_fit <- lmFit(Wx, Dmatrix, method = "robust")
lm_fit <- contrasts.fit(lm_fit, Cmatrix)
lm_fit <- eBayes(lm_fit)

topT <- topTable(lm_fit, number = Inf)



pheatmap(Wx[rownames(topT)[1:100],])

i <- 0

i <- i+1
goName <- "megakaryocyte development"
boxplot(colMeans(CCLE_demeter_GOmatch[which(GxO[,goName] == 1),])~isAML, main = paste(goName))


CCLE_demeter_GOmatch[which(GxO[,goName] == 1),]






# Wx <- data.frame(Ataris_gn = rownames(Wx), Wx)
# Wx <- gather(Wx, Transcript, z, -Ataris_gn)
# ord2 <- order(abs(Wx$z), decreasing = T)
# Wx <- Wx[ord2,]
# Wx$pv <- 2*pnorm(-abs(Wx$z))
# Wx$adj.pv <- p.adjust(Wx$pv, "BH")
# 
# hist(Wx$pv,50)

# rownames(cors) <- NULL


# boxplot(CCLE_tpm_log2["ENST00000376509", iCL] ~ CCLE_ataris_bin["CFLAR", iCL])
# 
# boxplot(CCLE_tpm_log2[iTr[1], iCL]~ CCLE_ataris_bin[iGn[1], iCL])
# 
# wilcox.test(CCLE_tpm_log2[iTr[1], iCL]~ CCLE_ataris_bin[iGn[1], iCL])
# 
# Wilcoxon.z.matrix(CCLE_tpm_log2[iTr[1], iCL, drop=F], t(CCLE_ataris_bin[iGn[1], iCL, drop=F]))
# 
# pnorm(-1.19)



