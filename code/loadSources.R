loadSources <- function(){
  library(dplyr)
  library(readr)
  library(Matrix)
  library(matrixStats)
  library(psych)
  library(tidyr)
  library(ggplot2)
  library(pheatmap)
  library(RColorBrewer)
  library(progress)
  library(biomaRt)
  
  
  plotAtaris <- function (CCLE_tpm_log2, CCLE_ataris, CCLE_sample_info, getBM, tr, gn, method = "pearson", n = NULL){
    
    info_t <- getBM[which(getBM$Transcript_ID==tr),]
    A <- data.frame(log2_TPM = CCLE_tpm_log2[tr, ], 
                    Ataris = CCLE_ataris[gn, ], 
                    Disease_C = CCLE_sample_info$Disease_Complete,
                    Disease = CCLE_sample_info$Disease)
    
    pv <- cor.test(CCLE_tpm_log2[tr, ], CCLE_ataris[gn, ],method = method)$p.value
    cor <- cor(CCLE_tpm_log2[tr, ], CCLE_ataris[gn, ],method = method)
    
    a <- (ggplot(A, aes(log2_TPM, Ataris)) + 
            geom_point(aes(color = Disease), size =3) +
            geom_text(aes(label=Disease, color = Disease), size = 3, hjust=0, vjust=-1) +
            ggtitle(sprintf("%s. Transcript: %s  |  Ataris: %s", n, info_t$Transcrip_name, gn)) +
            geom_text(aes(-Inf, Inf, label= sprintf("P.v = %.2e \n cor = %.2f", pv, cor)),
                      vjust = "inward",
                      hjust = "inward", color =2, size = 3.5))
    print(a)
    # return(a)
  }
  
  
  corPv <- function (CCLE_ataris, CCLE_tpm_log2, nCors = 1000, method = "pearson"){
    pb <- progress_bar$new(
      format = "  downloading [:bar] :percent progress: :eta",
      total = nCors, clear = FALSE, width= 80)
    
    cors <- cor(t(CCLE_ataris), t(CCLE_tpm_log2) , method = method)  
    cors <- data.frame(Ataris_gn = rownames(cors), cors)
    cors <- gather(cors, Transcript, cors, -Ataris_gn)
    ord2 <- order(abs(cors$cors), decreasing = T)
    cors <- cors[ord2[1:nCors],]
    rownames(cors) <- NULL
    
    pv <- rep(NA, nCors)
    for (p in 1:nCors){
      pb$tick()
      TR <- cors$Transcript[p]
      GN <- cors$Ataris_gn[p]
      pv[p] <- cor.test(CCLE_ataris[GN, ], CCLE_tpm_log2[TR, ], method = method)$p.value
    }
    
    cors$pv <- pv
    cors$adj.pv <- p.adjust(pv, "BH")
    return(cors)
  }
  
  
}